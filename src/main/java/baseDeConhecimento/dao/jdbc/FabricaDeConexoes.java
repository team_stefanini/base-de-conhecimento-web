package baseDeConhecimento.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FabricaDeConexoes {
private Connection conexao;
	
	public Connection getConexao() {
		return conexao;
	}
	
	/**
	 * Abre a conex�o com o banco de dados
	 * @throws ClassNotFoundException
	 * @throws SQLException 
	 */
	public void abrir() throws ClassNotFoundException, SQLException{
		//carregando a classe drive na memoria 
		Class.forName("com.mysql.jdbc.Driver");
		
		// informa��o da conex�o
		final String servidor = "localhost",
				BANCO_DE_DADOS = "basedeconhecimento",
				USUARIO = "root",
				SENHA = "root";
		
		final int PORTA = 3306;
		
		final String URL = "jdbc:mysql://"+ servidor +":"+ PORTA+"/"+BANCO_DE_DADOS;
		
		//Criando a conex�o e aplicando-a no objeto
		//Para se conectar precisamos informar a URL, usu�rio
		//de acesso ea senha 
		conexao = DriverManager.getConnection(URL,USUARIO,SENHA);	
	}
	
	/**
	 * Fexa a conex�o com o banco de dados
	 * @throws SQLException
	 */
	public void fechar() {
		if (conexao != null) {
			
			try {
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conexao = null;
		}
	}
	
}

package baseDeConhecimento.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoImagem;
import baseDeConhecimento.model.Imagem;

@Repository
public class JPAImagem implements DaoImagem {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Imagem buscar(Long id) {
		return manager.find(Imagem.class, id);
	}

	@Override
	public List<Imagem> buscarTodos() {
		TypedQuery<Imagem> query = manager.createQuery("from Imagem", Imagem.class);
		return query.getResultList();
	}

	@Override
	public List<Imagem> buscarTodosInativos() {
		return null;
	}

	@Transactional
	@Override
	public void inserir(Imagem obj) {
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Imagem obj) {
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Imagem obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	public void desativar(Imagem obj) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deletarImagens(List<Long> idImagens) {
		for (Long imagem : idImagens) {
			manager.remove(buscar(imagem));
		}
	}

	@Override
	public List<Imagem> buscarImagensEmpresa(Long id) {
		TypedQuery<Imagem> query = manager.createQuery("select i from Imagem i where i.empresa.id = :id", Imagem.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

}

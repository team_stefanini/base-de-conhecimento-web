package baseDeConhecimento.model;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class RelatorioProcedimento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(columnDefinition = "Date")
	private Calendar dataEmissao = Calendar.getInstance();

	private Integer qtdProcedimentosSistema;

	@Column(columnDefinition = "MEDIUMBLOB")
	private HashMap<String, List<QtdProcedimentoAnalista>> qtdProcedimentosAcessadosAnalista;

	private boolean status;

	private Calendar dataVencimento;
	
	@ManyToOne
	private Usuario analista;
	
	@ManyToOne
	private Empresa empresa;
	
	@ManyToOne
	private Usuario usuarioGerouRelatorio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Integer getQtdProcedimentosSistema() {
		return qtdProcedimentosSistema;
	}

	public void setQtdProcedimentosSistema(Integer qtdProcedimentosSistema) {
		this.qtdProcedimentosSistema = qtdProcedimentosSistema;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> getQtdProcedimentosAcessadosAnalista() {
		return qtdProcedimentosAcessadosAnalista;
	}

	public void setQtdProcedimentosAcessadosAnalista(
			HashMap<String, List<QtdProcedimentoAnalista>> qtdProcedimentosAcessadosAnalista) {
		this.qtdProcedimentosAcessadosAnalista = qtdProcedimentosAcessadosAnalista;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Calendar getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Calendar dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Calendar getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Calendar dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public Usuario getAnalista() {
		return analista;
	}

	public void setAnalista(Usuario analista) {
		this.analista = analista;
	}

	public Usuario getUsuarioGerouRelatorio() {
		return usuarioGerouRelatorio;
	}

	public void setUsuarioGerouRelatorio(Usuario usuarioGerouRelatorio) {
		this.usuarioGerouRelatorio = usuarioGerouRelatorio;
	}
	
	
	

}

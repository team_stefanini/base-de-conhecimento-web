package baseDeConhecimento.model;

public enum TipoUsuario {
	ADMINISTRADOR,
	COORDENADOR,
	ANALISTA
}

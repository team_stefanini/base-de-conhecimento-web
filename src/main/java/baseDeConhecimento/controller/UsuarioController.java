package baseDeConhecimento.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWTVerifier;

import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.TokenJWT;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;
import baseDeConhecimento.view.InformacoesUsuario;

@CrossOrigin
@RestController
public class UsuarioController {
	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	@RequestMapping(value = "/usuario", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> inserirUsuario(@RequestBody Usuario usuario, HttpServletRequest request) {
		try {

			List<Empresa> empresas = new ArrayList<Empresa>();
			Usuario usuarioMula = new Usuario();

			if (usuario.getNome().trim().isEmpty() || usuario.getEmail().isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			}

			// garante que a empresa n�o vai ser cadastrada nulla
			if (!(usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR))) {
				if (usuario.getEmpresa().size() <= 0 || usuario.getEmpresa().isEmpty()) {
					// 601 N�o cont�m empresa no usuario cadastrado
					return ResponseEntity.status(601).body(null);
				}
			}
			if (validarPermissoes(request, usuario)) {
				Usuario usuarioBuscado = daoUsuario.buscarPorEmail(usuario.getEmail());
				if (usuarioBuscado == null) {
					usuario.setSenha(Util.encryptPassword(usuario.getSenha()));

					daoUsuario.inserir(usuario);
					usuarioMula = daoUsuario.buscar(usuario.getId());
					return ResponseEntity.created(new URI("/usuario/" + usuarioMula.getId())).body(usuarioMula);
				} else {
					// C�digo 600 significa que o email j� existe
					return ResponseEntity.status(600).body(null);
				}

			} else {
				return new ResponseEntity<Usuario>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {

			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);

		} catch (Exception e) {

			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Usuario>> listarUsuario(HttpServletRequest request) {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			List<Usuario> usuarios = new ArrayList<Usuario>();
			List<Usuario> listUsuarios = new ArrayList<Usuario>();
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				usuarios.addAll(daoUsuario.buscarUsuariosUsuariosPorEmpresa(empresa.getId()));
			}

			for (Usuario usuario : usuarios) {
				if (!listUsuarios.contains(usuario) && usuario.getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
					listUsuarios.add(usuario);
				}
			}

			return ResponseEntity.ok().body(listUsuarios);
		} else if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return ResponseEntity.ok().body(daoUsuario.buscarTodos());
		} else {
			return new ResponseEntity<List<Usuario>>(HttpStatus.FORBIDDEN);
		}
	}

	/*
	 * METODO QUE BUSCA TODOS OS USUARIOS INATIVOS NO SISTEMA
	 */
	@RequestMapping(value = "/usuario/inativos", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Usuario>> listarUsuariosInativos(HttpServletRequest request) {

		return ResponseEntity.ok().body(daoUsuario.buscarTodosInativos());

	}

	@RequestMapping(value = "/usuario/{idUsuario}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> buscarUsuario(@PathVariable Long idUsuario, HttpServletRequest request) {

		Usuario u = daoUsuario.buscar(idUsuario);

		if (validarPermissoes(request, u)) {
			return ResponseEntity.ok().body(daoUsuario.buscar(idUsuario));
		} else {
			return new ResponseEntity<Usuario>(HttpStatus.FORBIDDEN);
		}
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<Usuario> alterarUsuario(@RequestBody Usuario usuario,
			HttpServletRequest request) {

		Usuario usuarioBuscado = daoUsuario.buscar(usuario.getId());

		try {
			if (validarPermissoes(request, usuarioBuscado)) {
				/*
				 * testa se o usuario esta se alterando e seta as informa��es
				 * necessarias para ser alterado
				 */
				if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
					if (Util.getUsuarioLogado(request).getId() == usuarioBuscado.getId()) {
						if (usuarioBuscado.getSenha().equals(Util.encryptPassword(usuario.getSenha()))) {
							usuario.setSenha(usuarioBuscado.getSenha());
							daoUsuario.alterar(usuario);
							return ResponseEntity.status(HttpStatus.OK).body(usuario);
						} else {
							return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
						}
					}
					usuario.setSenha(usuarioBuscado.getSenha());
					daoUsuario.alterar(usuario);
					return ResponseEntity.status(HttpStatus.OK).body(usuario);
				}

				if (Util.getUsuarioLogado(request).getId() == usuario.getId()) {
					if (usuarioBuscado.getSenha().equals(Util.encryptPassword(usuario.getSenha()))) {
						usuario.setSenha(usuarioBuscado.getSenha());
						usuario.setEmpresa(usuarioBuscado.getEmpresa());
						usuario.setTipoUsuario(usuarioBuscado.getTipoUsuario());
						daoUsuario.alterar(usuario);
						return ResponseEntity.status(HttpStatus.OK).body(usuario);
					} else {

						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
					}
				} else {
					if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.COORDENADOR)
							&& usuarioBuscado.getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
						usuario.setSenha(usuarioBuscado.getSenha());
						daoUsuario.alterar(usuario);
						return ResponseEntity.status(HttpStatus.OK).body(usuario);

					} else {
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
					}
				}
			} else {
				return new ResponseEntity<Usuario>(HttpStatus.UNAUTHORIZED);
			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletarUsuario(@PathVariable Long id, HttpServletRequest request) {
		Usuario usuario = daoUsuario.buscar(id);

		if (usuario != null) {
			if (validarPermissoes(request, usuario)) {
				daoUsuario.deletar(usuario);
				return ResponseEntity.noContent().build();
			} else {
				return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
			}
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> desativarUsuario(@PathVariable Long id, HttpServletRequest request) {
		Usuario usuario = daoUsuario.buscar(id);

		if (usuario != null) {
			if (validarPermissoes(request, usuario)) {
				daoUsuario.desativar(usuario);
				return ResponseEntity.noContent().build();
			} else {
				return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
			}
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

	}
	
	/***
	 * Troca a senha do us�ario
	 * @param senha
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/usuario/alterarsenha/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> alterarSenha(@RequestBody InformacoesUsuario usuario,  HttpServletRequest request) {
		try {
			Usuario usuariobuscado = Util.getUsuarioLogado(request);
			if (usuariobuscado.getSenha().equals(Util.encryptPassword(usuario.getSenhaAntiga()))){
				usuariobuscado.setSenha(Util.encryptPassword(usuario.getNovaSenha()));
				daoUsuario.alterar(usuariobuscado);
				return ResponseEntity.status(HttpStatus.OK).body(usuariobuscado);
			}else{
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
			}
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<InformacoesUsuario> logar(@RequestBody Usuario usuario, HttpServletRequest request) {

		usuario = daoUsuario.logar(usuario);
		InformacoesUsuario informacoesUsuario = new InformacoesUsuario();
		if (usuario != null) {
			TokenJWT tokenJwt = TokenJWT.gerarToken(usuario, request);
			informacoesUsuario.setToken(tokenJwt);
			informacoesUsuario.setUsuario(usuario);
			return ResponseEntity.ok(informacoesUsuario);
		} else {
			return new ResponseEntity<InformacoesUsuario>(HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(value = "/usuario/verificarEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Usuario> buscarPorEmail(@RequestParam("email") String email) {
		if (email != null) {
			Usuario usuario = daoUsuario.buscarPorEmail(email);
			return ResponseEntity.ok().body(usuario);
		} else {
			return new ResponseEntity<Usuario>(HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/usuario/redefinirSenha", method = RequestMethod.POST)
	public ResponseEntity<Void> alterarSenha(@RequestParam("id") Long id, @RequestParam("resposta") String resposta,
			@RequestParam("senha") String senha) {

		if (senha.equals("") || id == null || resposta.equals("")) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		Usuario usuarioBuscado = daoUsuario.buscar(id);

		if (usuarioBuscado != null) {

			if (usuarioBuscado.getRespostaSeguranca().equals(resposta.trim())) {
				usuarioBuscado.setSenha(Util.encryptPassword(senha.trim()));
				daoUsuario.alterar(usuarioBuscado);
				return new ResponseEntity<Void>(HttpStatus.OK);
			} else {
				return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
			}

		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}

	// validar se o Usuario tem permiss�o para cadastrar usuario
	private boolean validarPermissoes(HttpServletRequest request, Usuario usuario) {

		try {
			Usuario usuarioLogado = Util.getUsuarioLogado(request);
			if (usuario != null) {
				// Verifica o tipo de usuario que o usuario esta tentando
				// cadastrar
				if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)
						&& (usuario.getTipoUsuario().equals(TipoUsuario.COORDENADOR)
								|| usuario.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR))) {
					if (usuarioLogado.getId() == usuario.getId()) {
						return true;
					} else {
						return false;
					}
				} else if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
					if (usuarioLogado.getId() == usuario.getId()) {
						return true;
					} else {
						return false;
					}
				}

				// garante que o coordenador somente cadastre usu�rios de suas
				// empresas.
				if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

					List<Empresa> empresasCoordenador = usuarioLogado.getEmpresa();
					List<Empresa> empresaUsuario = usuario.getEmpresa();
					
					for (Empresa empresaU : empresaUsuario) {
						if (empresasCoordenador.contains(empresaU)){
							return true;
						}
					}
					return false;
				}
			} else if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// se as permiss�es estiverem corretas,libera para cadastrar
		return true;
	}

}

package baseDeConhecimento.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;
import baseDeConhecimento.util.ValidacaoRelatorio;

@Repository
public class JPAUsuario implements DaoUsuario {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Usuario buscar(Long id) {
		return manager.find(Usuario.class, id);

	}

	@Override
	public List<Usuario> buscarTodos() {
		TypedQuery<Usuario> query = manager.createQuery("from Usuario where status = true", Usuario.class);

		return query.getResultList();
	}

	@Override
	public List<Usuario> buscarTodosInativos() {
		TypedQuery<Usuario> query = manager.createQuery("from Usuario where status = false", Usuario.class);

		return query.getResultList();
	}

	public Usuario buscarPorEmail(String email) {
		try{
		TypedQuery<Usuario> query = manager.createQuery("from Usuario where email = :email", Usuario.class);
		query.setParameter("email", email);

		return query.getSingleResult();
		}catch (Exception e) {
			return null;
		}

	}

	@Override
	@Transactional
	public void inserir(Usuario obj) {
		manager.persist(obj);
	}

	@Override
	@Transactional
	public void alterar(Usuario obj) {
		manager.merge(obj);
	}

	@Override
	@Transactional
	public void deletar(Usuario obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	public Usuario logar(Usuario usuario) {

		try {
			TypedQuery<Usuario> query = manager.createQuery(
					"from Usuario where email = :email and senha = :senha and status = true", Usuario.class);

			query.setParameter("email", usuario.getEmail());
			query.setParameter("senha", Util.encryptPassword(usuario.getSenha()));

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Usuario> buscarUsuarioPorEmpresa(Long id) {
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u join u.empresa e where e.id = :id group by u.id",
				Usuario.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	
	@Override
	public List<Usuario> buscarUsuariosAnalistasPorEmpresa(Long id) {
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u join u.empresa e where e.id = :id and u.tipoUsuario = 2",
				Usuario.class);
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	
	@Override
	public List<Usuario> buscarUsuariosUsuariosPorEmpresa(Long id) {
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u join u.empresa e where e.id = :id ",
				Usuario.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	@Transactional
	public void desativar(Usuario obj) {
		obj.setStatus(false);
		manager.merge(obj);
	}

	@Override
	public List<Usuario> buscarTodosSistema() {
		TypedQuery<Usuario> query = manager.createQuery("from Usuario", Usuario.class);

		return query.getResultList();
	}
}
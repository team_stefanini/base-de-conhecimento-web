package baseDeConhecimento.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Procedimento;

@Repository
public class JPAProcedimento implements DaoProcedimento {
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Procedimento buscar(Long id) {
		return manager.find(Procedimento.class, id);
	}

	@Override
	public Procedimento buscarInativos(Long id) {
		TypedQuery<Procedimento> query = manager
				.createQuery("from Procedimento where status = false and id = :idProcedimento", Procedimento.class);
		query.setParameter("idProcedimento", id);

		return query.getSingleResult();
	}

	@Override
	public List<Procedimento> buscarTodos() {
		try {
			TypedQuery<Procedimento> query = manager.createQuery("from Procedimento where status = true",
					Procedimento.class);

			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarTodosInativos() {
		TypedQuery<Procedimento> query = manager.createQuery("from Procedimento where status = false",
				Procedimento.class);

		return query.getResultList();
	}

	@Transactional
	@Override
	public void inserir(Procedimento obj) {
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Procedimento obj) {
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Procedimento obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	public List<Procedimento> buscarProcedimentosRelacionados(Long idCategoria, List<Long> categorias) {
		try {
			List<Long> ids = new ArrayList<>();
			ids.add(idCategoria);

			if (categorias != null) {
				for (Long idFilho : categorias) {
					ids.add(idFilho);
				}
			}

			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true and p.status = true order by vencimento",
					Procedimento.class);
			query.setParameter("categorias", ids);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarProcedimentosRelacionadosPorEmpresa(List<Long> categorias) {
		try {
			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true and p.status = true",
					Procedimento.class);
			//TODO 
			query.setParameter("categorias", categorias);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarProcedimentosInativosRelacionadosPorEmpresa(List<Long> categorias) {
		try {
			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true and p.status = false",
					Procedimento.class);
			query.setParameter("categorias", categorias);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<Procedimento> buscarProcedimentosEmpresaUsuarioLogado(List<Long> idEmpresas) {
		try {
			
			String Sql = "select p from Procedimento p join p.categoria c where c.empresa.id in (:idEmpresas) and c.status = true and p.status = true";

			
			TypedQuery<Procedimento> query = manager.createQuery(
					Sql,
					Procedimento.class);
			query.setParameter("idEmpresas", idEmpresas);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("======================================================================================================================================================");
			System.out.println("JPA PROCEDIMENTO CACH NOW , AGORA , NESTE MOMENTO ");
			System.out.println("======================================================================================================================================================");
			
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarProcedimentosRelacionadosParaAnalista(Long idCategoria, List<Long> categorias) {
		try {
			List<Long> ids = new ArrayList<>();
			ids.add(idCategoria);

			if (categorias != null) {
				for (Long idFilho : categorias) {
					ids.add(idFilho);
				}
			}

			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true order by dataCadastro",
					Procedimento.class);
			query.setParameter("categorias", ids);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarProcedimentosRelacionadosPorEmpresaParaAnalista(List<Long> categorias) {
		try {

			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true order by p.dataCadastro",
					Procedimento.class);
			query.setParameter("categorias", categorias);
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional
	public void desativar(Procedimento obj) {
		obj.setStatus(false);
		manager.merge(obj);
	}

	@Override
	public List<Procedimento> buscarTodosProcedimentosPelasCategorias(Categoria[] categorias) {
		try {
			List<Long> ids = new ArrayList<>();
			if (categorias != null) {
				for (Categoria idFilho : categorias) {
					ids.add(idFilho.getId());
				}
			}

			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p join p.categoria c where c.id in :categorias and c.status = true and p.status = true",
					Procedimento.class);
			query.setParameter("categorias", ids);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarQtdProcedimentosUsuarios(Long id) {
		try {
			TypedQuery<Integer> query = manager
					.createQuery("select p.listaUsuario.size from Procedimento p where p.id = :id", Integer.class);
			query.setParameter("id", id);
			System.out.println("TAMANHO>>>>>>>>>>>>>>>>>>>>>>>>>>>" + query.getSingleResult());
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<Procedimento> buscarLegiveis() {
		TypedQuery<Procedimento> query = manager.createQuery("from Procedimento where elegivel = true",
				Procedimento.class);
		return query.getResultList();
	}

	@Override
	public List<Procedimento> buscarIlegiveis() {
		TypedQuery<Procedimento> query = manager.createQuery("from Procedimento where elegivel = false",
				Procedimento.class);
		return query.getResultList();
	}

	public List<Procedimento> buscarTodosEmpresaCoordenador(List<Long> idEmpresa) {
		TypedQuery<Procedimento> query = manager.createQuery(
				"select p from Procedimento p inner join Categoria c on p.categoria.id = c.id where c.empresa.id in :ids",
				Procedimento.class);
		query.setParameter("ids", idEmpresa);
		return query.getResultList();
	}

	@Override
	public Procedimento buscarProcedimentoPorTitulo(Long idEmpresa, String titulo) {
		try {
			TypedQuery<Procedimento> query = manager.createQuery(
					"select p from Procedimento p inner join Categoria c on p.categoria.empresa.id = idEmpresa where p.titulo = titulo",
					Procedimento.class);
			query.setParameter("idEmpresa", idEmpresa);
			query.setParameter("titulo", titulo);

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
		
	}
}

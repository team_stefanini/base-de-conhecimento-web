package baseDeConhecimento.view;

import baseDeConhecimento.model.TokenJWT;
import baseDeConhecimento.model.Usuario;

public class InformacoesUsuario {

	private Usuario usuario;

	private TokenJWT token;
	
	private String senhaAntiga;
	
	private String novaSenha;

	public TokenJWT getToken() {
		return token;
	}

	public void setToken(TokenJWT token) {
		this.token = token;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getSenhaAntiga() {
		return senhaAntiga;
	}

	public void setSenhaAntiga(String senhaAntiga) {
		this.senhaAntiga = senhaAntiga;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	
	
	

}

package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Procedimento;

public interface DAO<T> {

	public T buscar(Long id);

	public List<T> buscarTodos();
	
	public List<T> buscarTodosInativos();

	public void inserir(T obj);

	public void alterar(T obj);
	
	public void desativar(T obj);

	public void deletar(T obj);
}

package baseDeConhecimento.controller;

import java.net.URI;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import baseDeConhecimento.dao.DaoGrupoSolucao;
import baseDeConhecimento.model.GrupoSolucao;

@CrossOrigin
@RestController
public class GrupoSolucaoController {
	@Autowired
	@Qualifier("JPAGrupoSolucao")
	private DaoGrupoSolucao daoGrupoSolucao;

	/*
	 * METODO QUE INSERE UM GRUPO DE SOLU��O
	 */

	@RequestMapping(value = "/grupo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GrupoSolucao> inserirGrupoSolucao(@RequestBody GrupoSolucao grupoSolucao) {
		try {
			GrupoSolucao grupoSolucao2 = daoGrupoSolucao.buscarPorNome(grupoSolucao.getNome());
			if (grupoSolucao2 != null) {
				// Status 600 grupo de solu��o j� existente
				return ResponseEntity.status(600).body(null);
			}

			daoGrupoSolucao.inserir(grupoSolucao);
			return ResponseEntity.created(URI.create("/gruposolucao/" + grupoSolucao.getId())).body(grupoSolucao);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<GrupoSolucao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<GrupoSolucao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * METODO QUE BUSCA UM GRUPO DE SOLU��ES ESPECIFICO
	 */

	@RequestMapping(value = "grupo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public GrupoSolucao buscarGrupoSolucao(@PathVariable Long id) {
		return daoGrupoSolucao.buscar(id);
	}

	/*
	 * METODO QUE BUSCA TODOS OS GRUPOS DE SOLU��O
	 */

	@RequestMapping(value = "/grupo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<GrupoSolucao> listarGrupoSolucao() {
		return daoGrupoSolucao.buscarTodos();
	}

	/*
	 * METODO QUE BUSCA OS GRUPOS DE SOLU��O INATIVOS NO SISTEMA
	 */

	@RequestMapping(value = "/grupo/inativos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<GrupoSolucao> listarGrupoSolucaoInativos() {
		return daoGrupoSolucao.buscarTodosInativos();
	}

	/*
	 * METODO QUE DELETA UM GRUPO DE SOLU��O
	 */

	@RequestMapping(value = "grupo/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirGrupoSolucao(@PathVariable Long id) {
		daoGrupoSolucao.deletar(daoGrupoSolucao.buscar(id));
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE DESATIVA UM GRUPO DE SOLU��O
	 */

	@RequestMapping(value = "grupo/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> desativarGrupoSolucao(@PathVariable Long id) {
		daoGrupoSolucao.desativar(daoGrupoSolucao.buscar(id));
		return ResponseEntity.noContent().build();
	}
	
	/*
	 * METODO QUE ALTERA UM GRUPO DE SOLU��O
	 */

	@RequestMapping(value = "/grupo", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GrupoSolucao> alterarGrupoSolucao(@RequestBody GrupoSolucao grupoSolucao) {
		try {
			daoGrupoSolucao.alterar(grupoSolucao);
			return ResponseEntity.created(URI.create("/gruposolucao/" + grupoSolucao.getId())).body(grupoSolucao);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<GrupoSolucao>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<GrupoSolucao>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

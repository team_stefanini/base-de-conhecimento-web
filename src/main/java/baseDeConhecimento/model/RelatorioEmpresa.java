package baseDeConhecimento.model;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class RelatorioEmpresa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@Column(columnDefinition = "DATE")
	private Calendar dataEmissao = Calendar.getInstance();
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Usuario> usuarios;
	private Integer qntEmpresas;
	@Column(columnDefinition = "LONGBLOB")
	private HashMap<String, HashMap<Long, Long>> qntUsuariosPorEmpresa;
	private Integer qntUsuarios;
	private Integer qntUsuariosNaEmpresa;
	private Integer qntAnalistas;
	private Integer qntCoodenadores;
	private Integer qntCategorias;
	private Integer qntCategoriasENiveisVazios;
	private Integer qntProcedimentos;
	@Fetch(FetchMode.SUBSELECT)
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> caminhoCategoriasSemProcedimento;
	@Column(columnDefinition = "LONGBLOB")
	private HashMap<String, HashMap<Long, Long>> qtdProcedimentosAcessados;
	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.EAGER)
	private List<QtdProcedimentoAnalista> qtdProcedimentosAnalista;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Calendar dataVencimento;
	// ATRIBUTO DO SISTEMA INTEIRO
	private Integer qntCategoriasPai;
	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Categoria> categoriasPai;
	@ManyToOne
	private Usuario usuarioGerouRelatorio;
	@ManyToOne
	private Empresa empresa;
	private Integer qtdProcedimentosSistema;
	@Column(columnDefinition = "MEDIUMBLOB")
	private HashMap<String, List<QtdProcedimentoAnalista>> qtdProcedimentosAcessadosAnalista;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Calendar dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Integer getQntEmpresas() {
		return qntEmpresas;
	}

	public void setQntEmpresas(Integer qntEmpresas) {
		this.qntEmpresas = qntEmpresas;
	}

	public Integer getQntUsuarios() {
		return qntUsuarios;
	}

	public void setQntUsuarios(Integer qntUsuarios) {
		this.qntUsuarios = qntUsuarios;
	}

	public Integer getQntAnalistas() {
		return qntAnalistas;
	}

	public void setQntAnalistas(Integer qntAnalistas) {
		this.qntAnalistas = qntAnalistas;
	}

	public Integer getQntCoodenadores() {
		return qntCoodenadores;
	}

	public void setQntCoodenadores(Integer qntCoodenadores) {
		this.qntCoodenadores = qntCoodenadores;
	}

	public Integer getQntCategorias() {
		return qntCategorias;
	}

	public void setQntCategorias(Integer qntCategorias) {
		this.qntCategorias = qntCategorias;
	}

	public Integer getQntCategoriasENiveisVazios() {
		return qntCategoriasENiveisVazios;
	}

	public void setQntCategoriasENiveisVazios(Integer qntCategoriasENiveisVazios) {
		this.qntCategoriasENiveisVazios = qntCategoriasENiveisVazios;
	}

	public Integer getQntProcedimentos() {
		return qntProcedimentos;
	}

	public void setQntProcedimentos(Integer qntProcedimentos) {
		this.qntProcedimentos = qntProcedimentos;
	}

	public HashMap<String, HashMap<Long, Long>> getQntUsuariosPorEmpresa() {
		return qntUsuariosPorEmpresa;
	}

	public void setQntUsuariosPorEmpresa(HashMap<String, HashMap<Long, Long>> qntUsuariosPorEmpresa) {
		this.qntUsuariosPorEmpresa = qntUsuariosPorEmpresa;
	}

	public Integer getQntUsuariosNaEmpresa() {
		return qntUsuariosNaEmpresa;
	}

	public void setQntUsuariosNaEmpresa(Integer qntUsuariosNaEmpresa) {
		this.qntUsuariosNaEmpresa = qntUsuariosNaEmpresa;
	}

	public List<String> getCaminhoCategoriasSemProcedimento() {
		return caminhoCategoriasSemProcedimento;
	}

	public void setCaminhoCategoriasSemProcedimento(List<String> caminhoCategoriasSemProcedimento) {
		this.caminhoCategoriasSemProcedimento = caminhoCategoriasSemProcedimento;
	}

	public HashMap<String, HashMap<Long, Long>> getQtdProcedimentosAcessados() {
		return qtdProcedimentosAcessados;
	}

	public void setQtdProcedimentosAcessados(HashMap<String, HashMap<Long, Long>> qtdProcedimentosAcessados) {
		this.qtdProcedimentosAcessados = qtdProcedimentosAcessados;
	}

	public List<QtdProcedimentoAnalista> getQtdProcedimentosAnalista() {
		return qtdProcedimentosAnalista;
	}

	public void setQtdProcedimentosAnalista(List<QtdProcedimentoAnalista> qtdProcedimentosAnalista) {
		this.qtdProcedimentosAnalista = qtdProcedimentosAnalista;
	}

	public Calendar getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Calendar dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Integer getQntCategoriasPai() {
		return qntCategoriasPai;
	}

	public void setQntCategoriasPai(Integer qntCategoriasPai) {
		this.qntCategoriasPai = qntCategoriasPai;
	}

	public List<Categoria> getCategoriasPai() {
		return categoriasPai;
	}

	public void setCategoriasPai(List<Categoria> categoriasPai) {
		this.categoriasPai = categoriasPai;
	}

	public Usuario getUsuarioGerouRelatorio() {
		return usuarioGerouRelatorio;
	}

	public void setUsuarioGerouRelatorio(Usuario usuarioGerouRelatorio) {
		this.usuarioGerouRelatorio = usuarioGerouRelatorio;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Integer getQtdProcedimentosSistema() {
		return qtdProcedimentosSistema;
	}

	public void setQtdProcedimentosSistema(Integer qtdProcedimentosSistema) {
		this.qtdProcedimentosSistema = qtdProcedimentosSistema;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> getQtdProcedimentosAcessadosAnalista() {
		return qtdProcedimentosAcessadosAnalista;
	}

	public void setQtdProcedimentosAcessadosAnalista(
			HashMap<String, List<QtdProcedimentoAnalista>> qtdProcedimentosAcessadosAnalista) {
		this.qtdProcedimentosAcessadosAnalista = qtdProcedimentosAcessadosAnalista;
	}
	
	

}
package baseDeConhecimento.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.dao.jpa.JPARelatorioEmpresa;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Procedimento;
import baseDeConhecimento.model.RelatorioEmpresa;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;
import baseDeConhecimento.util.ValidacaoRelatorio;
import baseDeConhecimento.view.BuscarRelatorioPorData;

@RestController
@CrossOrigin
public class GerarRelatorioControllerEmpresa {
	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	@Autowired
	@Qualifier("JPACategoria")
	private DaoCategoria daoCategoria;

	@Autowired
	@Qualifier("JPAProcedimento")
	private DaoProcedimento daoProcedimento;

	@Autowired
	private JPARelatorioEmpresa daoRelatorioEmpresa;


	/*
	 * METODO QUE TRAZ A EMPRESA E A QUANTIDADE DE USUARIOS LIGADOS A ELA,
	 * REFERENTE AO USUARIO
	 */

	@RequestMapping(value = "/relatorio/empresa", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RelatorioEmpresa> gerarRelatorioGeralEmpresa(HttpServletRequest request) {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		Calendar cal = Calendar.getInstance();

		// Adiciona um ao m�s, pois o Calendar conta os meses de 0 a 11
		cal.add(Calendar.MONTH, 1);

		// Lista de relatorios gerais das empresas
		RelatorioEmpresa relatorioDoMes = daoRelatorioEmpresa.buscarReltorioGeralEmpresa(cal, usuarioLogado);

		// Retorna ele com a data e o m�s atual
		cal = Calendar.getInstance();

		if (relatorioDoMes != null) {

			RelatorioEmpresa relatorio = ValidacaoRelatorio.verificarUsuarioEmpresa(true, usuarioLogado,
					relatorioDoMes);

			daoRelatorioEmpresa.alterar(relatorio);
			return ResponseEntity.ok().body(relatorio);

		}
		RelatorioEmpresa relatorio = new RelatorioEmpresa();

		RelatorioEmpresa relatorioEmpresa = ValidacaoRelatorio.verificarUsuarioEmpresa(false, usuarioLogado, relatorio);

		// Setando a data de vencimento
		// Colocando o vencimento de 6 meses
		cal.add(Calendar.MONTH, 6);
		relatorioEmpresa.setDataVencimento(cal);

		daoRelatorioEmpresa.inserir(relatorioEmpresa);

		return ResponseEntity.ok().body(relatorioEmpresa);
	}

	@RequestMapping(value = "/relatorio/empresa/{idEmpresa}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RelatorioEmpresa> gerarRelatorioPorEmpresa(HttpServletRequest request,
			@PathVariable Long idEmpresa) {
		try {
			if (validarPermissoes(idEmpresa, request)) {
				RelatorioEmpresa relatorioEmpresa = ValidacaoRelatorio.gerarRelatorioEmpresa(idEmpresa,
						Util.getUsuarioLogado(request));
				return ResponseEntity.ok().body(relatorioEmpresa);
			} else {
				return new ResponseEntity<RelatorioEmpresa>(HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<RelatorioEmpresa>(HttpStatus.BAD_REQUEST);
	}

	/*
	 * METODO QUE VERIFICA QUAIS RELATORIOS N�O EST�O VALIDOS
	 */
	// @Scheduled = fixedRate fala de quanto tempo o m�todo deve ser executado
	// em millisegundos
	@Scheduled(cron = "1 1 0 1 * *")
	public void verificarRelatorios() {

		List<RelatorioEmpresa> relatorioEmpresas = daoRelatorioEmpresa.buscarTodos();
		Calendar dataAtual = Calendar.getInstance();
		if (relatorioEmpresas.size() > 0) {
			for (RelatorioEmpresa relatorioEmpresa : relatorioEmpresas) {
				if (dataAtual.equals(relatorioEmpresa.getDataVencimento())) {
					daoRelatorioEmpresa.deletar(relatorioEmpresa);
				}
			}
		}
		List<Procedimento> procedimentos = daoProcedimento.buscarTodos();
		if (procedimentos != null) {
			for (Procedimento procedimento : procedimentos) {
				procedimento.getListaUsuario().clear();
				daoProcedimento.alterar(procedimento);
			}
		}

	}

	@RequestMapping(value = "/relatorio/data", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<RelatorioEmpresa> buscarRelatorioPorData(@RequestBody BuscarRelatorioPorData dados,
			HttpServletRequest request) {

		if (dados.getData() != null && dados.getIdEmpresa() != null) {

			try {
				SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
				
				// Valida as permiss�es do usuario logado
				if (validarPermissoes(dados.getIdEmpresa(), request)) {

					Calendar data = Calendar.getInstance();
					Integer cont = Integer.parseInt(dados.getData());
					for (int i = 0; i < cont; i++) {
						if (data.get(Calendar.MONTH) < 0) {
							data.add(Calendar.MONTH, 11);
							data.add(Calendar.MONTH, -1);
						}else{
							data.add(Calendar.MONTH, -1);
						}
					}
					
					//data.add(Calendar.MONTH, -Integer.parseInt(dados.getData()));
					data.add(Calendar.MONTH, 1);
					//data.setTime(fmt.parse(dados.getData()));
					fmt.format(data.getTime());
					System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + data.get(Calendar.MONTH));
					System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + fmt.format(data.getTime()));
					System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + dados.getData());
					RelatorioEmpresa relatorio = daoRelatorioEmpresa.buscarRelatorioPorData(data, dados.getIdEmpresa(),
							Util.getUsuarioLogado(request).getId());
					return ResponseEntity.ok().body(relatorio);
				} else {
					return new ResponseEntity<RelatorioEmpresa>(HttpStatus.UNAUTHORIZED);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (dados.getIdEmpresa() == null) {
			if (Util.getUsuarioLogado(request).getTipoUsuario() != TipoUsuario.ANALISTA) {
				SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
				Calendar data = Calendar.getInstance();
				Integer cont = Integer.parseInt(dados.getData());
				for (int i = 0; i < cont; i++) {
					if (data.get(Calendar.MONTH) < 0) {
						data.add(Calendar.MONTH, 11);
						data.add(Calendar.MONTH, -1);
					}else{
						data.add(Calendar.MONTH, -1);
					}
				}
				data.add(Calendar.MONTH, 1);
				//data.add(Calendar.MONTH, -Integer.parseInt(dados.getData()));
				//data.setTime(fmt.parse(dados.getData()));
				fmt.format(data.getTime());

				System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + data.get(Calendar.MONTH));
				System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + fmt.format(data.getTime()));
				System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + dados.getData());
				RelatorioEmpresa relatorio = daoRelatorioEmpresa.buscarRelatorioGeralPorData(data,
						Util.getUsuarioLogado(request).getId());
				return ResponseEntity.ok().body(relatorio);
			}
		}

		return new ResponseEntity<RelatorioEmpresa>(HttpStatus.BAD_REQUEST);

	}

	public boolean validarPermissoes(Long idEmpresa, HttpServletRequest request) throws Exception {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);

		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {
			Empresa empresa = daoEmpresa.buscar(idEmpresa);
			List<Empresa> empresasUsuario = usuarioLogado.getEmpresa();
			if (empresasUsuario.contains(empresa)) {
				return true;
			}
		} else if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return true;
		}
		return false;
	}
}

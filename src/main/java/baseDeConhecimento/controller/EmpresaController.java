package baseDeConhecimento.controller;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoImagem;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Imagem;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;

@CrossOrigin
@RestController
public class EmpresaController {

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	@Autowired
	@Qualifier("JPAImagem")
	private DaoImagem daoImagem;

	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	@Autowired
	private ServletContext servletContext;

	/*
	 * METODO QUE INSERE A EMPRESA COM A IMAGEM
	 */

	@RequestMapping(value = "/empresa", method = RequestMethod.POST)
	public ResponseEntity<Empresa> inserirEmpresa(
			@RequestParam(value = "file", required = false) MultipartFile inputFileImg,
			@RequestParam("nome") String nome) {

		try {
			Empresa empresa = daoEmpresa.buscarPorNome(nome);
			if (empresa != null) {
				// Status 600 empresa j� existente
				return ResponseEntity.status(600).body(null);
			}
			if (nome.trim() == null || nome.trim().isEmpty()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			}
			empresa = new Empresa();
			empresa.setNome(nome);
			daoEmpresa.inserir(empresa);

			if (inputFileImg != null && empresa.getId() != null) {
				try {
					// Cria os caminhos que ser�o salvos as imagens
					String caminhoDiretorio = servletContext.getRealPath("/");
					String caminhoimagem = "/logoEmpresa/empresa" + empresa.getId() + ".jpg";

					// cria um arquivo de destino para transferir a imagem para
					// o servidor.
					File destinationFile = new File(caminhoDiretorio + caminhoimagem);

					// testa se a extens�o da imagem � do tipo jpg ou jpeg se
					// n�o
					// for retorna um codigo de erro.
					/*
					if (!(inputFileImg.getContentType().contains("png")
							|| inputFileImg.getContentType().contains("jpg"))) {
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
					}
					*/

					// transfere a imagem para o diret�rio no servidor
					inputFileImg.transferTo(destinationFile);

					// busca a empresa e coloca o caminho da imagem no banco de
					// dados
					Imagem imagem = new Imagem();
					imagem.setCaminho(caminhoimagem);

					empresa.setImagemLogo(imagem);
					daoEmpresa.alterar(empresa);

					// return new ResponseEntity<Empresa>(HttpStatus.OK);
					return ResponseEntity.created(new URI("/empresa/" + empresa.getId())).body(empresa);

				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<Empresa>(HttpStatus.BAD_REQUEST);
				}
			} else {
				return ResponseEntity.created(new URI("/empresa/" + empresa.getId())).body(empresa);
			}
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Empresa>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Empresa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	/*
	 * METODO QUE BUSCA UMA EMPRESA EM ESPECIFICO
	 */
	@RequestMapping(value = "/empresa/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Empresa> buscarEmpresa(@PathVariable Long id, HttpServletRequest request) {
		
		List<Empresa> empresasUsuarioLogado = Util.getUsuarioLogado(request).getEmpresa();
		Empresa empresa = daoEmpresa.buscar(id);
		
		if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return ResponseEntity.status(HttpStatus.OK).body(empresa);
		}
		

		
		if (empresasUsuarioLogado.contains(empresa)) {
			return ResponseEntity.status(HttpStatus.OK).body(empresa);
		}
		
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	/*
	 * METODO QUE BUSCA AS IMAGENS DAQUELA EMPRESA EM ESPECIFICO
	 */
	@RequestMapping(value = "/empresa/{id}/imagem", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<Imagem>> buscarEmpresaImagens(@PathVariable Long id, HttpServletRequest request) {
		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		
		try {
			if (usuarioLogado.getEmpresa().contains(daoEmpresa.buscar(id)) || usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
				return ResponseEntity.status(HttpStatus.OK).body(daoImagem.buscarImagensEmpresa(id));
			}
		} catch (NullPointerException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	}

	
	/*
	 * INSERINDO VARIAS IMAGENS
	 */
	@RequestMapping(value = "/empresa/{id}/imagem", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
	public ResponseEntity<FileInfo> uploadFoto(@RequestParam("file") MultipartFile[] inputFileImg,
			@PathVariable Long id, HttpServletRequest request) {
		FileInfo fileInfo = new FileInfo();
		HttpHeaders headers = new HttpHeaders();

		if (inputFileImg.length != 0 && id != null) {
			try {
				
				// Cria a pasta da empresa no servidor caso ela n�o exista.
				String caminhoDiretorio = servletContext.getRealPath("/");
				Random rand = new Random();
				// busca a empresa
				Empresa empresa = daoEmpresa.buscar(id);
				
				
				
				if(!(Util.getUsuarioLogado(request).getEmpresa().contains(empresa))){
					if (!Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
						return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);	
					}
				}
				
				
				for (int i = 0; i < inputFileImg.length; i++) {

					String caminhoimagem = "/imgEmpresa/empresa" + id + "." + (rand.nextInt(1000) * id) + ".jpg";

					File destinationFile = new File(caminhoDiretorio + caminhoimagem);

					inputFileImg[i].transferTo(destinationFile);

					Imagem imagem = new Imagem();
					imagem.setCaminho(caminhoimagem);
					imagem.setEmpresa(empresa);
					daoImagem.inserir(imagem);
				}

				return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
			}
		} else {
			System.out.println("empresaController -> uploadFoto : caiu no ultimo else  !");
			return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/*
	 * METODO QUE LISTA TODAS AS EMPRESAS
	 */
	@RequestMapping(value = "/empresa", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Empresa> listarEmpresa(HttpServletRequest request) {

		Usuario usuario = Util.getUsuarioLogado(request);

		List<Empresa> empresas = null;

		// testa se o usuario � coordenador e se for retorna suas empresas
		if (usuario.getTipoUsuario() == TipoUsuario.COORDENADOR) {
			empresas = usuario.getEmpresa();
		} else {
			empresas = daoEmpresa.buscarTodos();
		}

		return empresas;

	}

	/*
	 * METODO QUE BUSCA TODAS AS EMPRESAS INATIVAS NO SISTEMA
	 */
	@RequestMapping(value = "/empresa/inativas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Empresa> listarEmpresasInativas(HttpServletRequest request) {

		return daoEmpresa.buscarTodosInativos();

	}

	/*
	 * METODO QUE ALTERA EMPRESA
	 */
	@RequestMapping(value = "/empresa/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Empresa> alterarEmpresa(@RequestParam(value = "nome") String nome,
			@PathVariable Long id, @RequestParam(value = "file", required = false) MultipartFile inputFileImg) {

		if (id != null) {

			try {
				Empresa empresa = daoEmpresa.buscar(id);
				empresa.setNome(nome);
				if (inputFileImg != null && empresa.getId() != null) {
					try {
						// Cria os caminhos que ser�o salvos as imagens
						String caminhoDiretorio = servletContext.getRealPath("/");
						String caminhoimagem = "/logoEmpresa/empresa" + empresa.getId() + ".jpg";

						// cria um arquivo de destino para transferir a imagem
						// para
						// o servidor.
						File destinationFile = new File(caminhoDiretorio + caminhoimagem);

						// testa se a extens�o da imagem � do tipo jpg ou jpeg
						// se
						// n�o
						// for retorna um codigo de erro.
						/*
						 * if (!(inputFileImg.getContentType().contains("jpg")
						 * || inputFileImg.getContentType().contains("jpeg"))) {
						 * 
						 * return
						 * ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
						 * null); }
						 * 
						 */
						// transfere a imagem para o diret�rio no servidor
						inputFileImg.transferTo(destinationFile);

						// busca a empresa e coloca o caminho da imagem no banco
						// de
						// dados
						Imagem imagem = new Imagem();
						imagem.setCaminho(caminhoimagem);

						empresa.setImagemLogo(imagem);
						empresa.setStatus(true);
						daoEmpresa.alterar(empresa);

						return ResponseEntity.status(HttpStatus.OK).body(empresa);

					} catch (Exception e) {
						e.printStackTrace();
						return new ResponseEntity<Empresa>(HttpStatus.BAD_REQUEST);
					}
				} else {
					empresa.setStatus(true);
					daoEmpresa.alterar(empresa);
					return ResponseEntity.status(HttpStatus.OK).body(empresa);
				}
			} catch (ConstraintViolationException e) {
				e.printStackTrace();
				return new ResponseEntity<Empresa>(HttpStatus.BAD_REQUEST);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Empresa>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<Empresa>(HttpStatus.BAD_REQUEST);
		}
	}

	/*
	 * METODO QUE EXCLUI A EMPRESA
	 */
	@RequestMapping(value = "/empresa/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirEmpresa(@PathVariable Long id) {
		daoEmpresa.deletar(daoEmpresa.buscar(id));
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE DESATIVA EMPRESA DO SISTEMA
	 */
	@RequestMapping(value = "/empresa/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> desativarEmpresa(@PathVariable Long id) {
		List<Usuario> usuariosDaEmpresa = daoUsuario.buscarUsuarioPorEmpresa(id);
		System.out.println("QUANTIDADE DA LISTA >>>>>>>>>>>>>>>>>>>>>>>> "+ usuariosDaEmpresa.size());
		for (Usuario usuario : usuariosDaEmpresa) {
			System.out.println("VOU REMOVER A EMPRESA DO USUARIO >>>>>>>>>>>>>>>>>>>>>> " + usuario.getId());
			System.out.println("VOU REMOVER A EMPRESA DO USUARIO QUANTIDADE >>>>>>>>>>>>>>>>>>>>>> " + usuario.getEmpresa().size());
			usuario.getEmpresa().remove(daoEmpresa.buscar(id));
			System.out.println("REMOVI A EMPRESA DO USUARIO >>>>>>>>>>>>>>>>>>>>>> " + usuario.getId());
			System.out.println("REMOVI A EMPRESA DO USUARIO QUANTIDADE >>>>>>>>>>>>>>>>>>>>>> " + usuario.getEmpresa().size());
			daoUsuario.alterar(usuario);
		}
		daoEmpresa.desativar(daoEmpresa.buscar(id));
		return ResponseEntity.noContent().build();
	}

}

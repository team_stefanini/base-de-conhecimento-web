package baseDeConhecimento.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import baseDeConhecimento.dao.DaoImagem;
import baseDeConhecimento.model.Imagem;

@CrossOrigin
@RestController
public class ImagemController {
	@Autowired
	@Qualifier("JPAImagem")
	private DaoImagem daoImagem;

	/*
	 * METODO QUE EXCLUI A IMAGEM
	 */
	@RequestMapping(value = "/imagem/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirImagemEmpresa(@PathVariable Long id) {
		daoImagem.deletar(daoImagem.buscar(id));
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE EXCLUI VARIAS IMAGENS
	 */
	@RequestMapping(value = "/imagem/{id}", method = RequestMethod.GET)
	public ResponseEntity<Imagem> buscarImagem(@PathVariable Long id) {
		Imagem img = daoImagem.buscar(id);
		return ResponseEntity.ok().body(img);
	}

}

package baseDeConhecimento.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.model.Categoria;

@Repository
public class JPACategoria implements DaoCategoria {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Categoria buscar(Long id) {
		return manager.find(Categoria.class, id);
	}

	@Override
	public List<Categoria> buscarTodos() {
		TypedQuery<Categoria> query = manager.createQuery("from Categoria where status = true", Categoria.class);

		return query.getResultList();
	}

	@Override
	public List<Categoria> buscarTodosInativos() {
		TypedQuery<Categoria> query = manager.createQuery("from Categoria where status = false", Categoria.class);

		return query.getResultList();
	}

	@Transactional
	@Override
	public void inserir(Categoria obj) {
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(Categoria obj) {
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(Categoria obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	public List<Categoria> buscarCategoriaPorEmpresa(Long id) {
		TypedQuery<Categoria> query = manager.createQuery(
				"from Categoria where empresa.id = :id and categoria is null and status = true ", Categoria.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public List<Categoria> buscarCategoriasPai() {
		TypedQuery<Categoria> query = manager.createQuery(
				"from Categoria where categoria is null and status = true ", Categoria.class);
		
		return query.getResultList();
	}
	
	@Override
	public List<Categoria> buscarNiveis(Long id) {
		TypedQuery<Categoria> query = manager.createQuery("from Categoria where categoria.id = :id and status = true",
				Categoria.class);
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	@Transactional
	public void desativar(Categoria obj) {
		manager.merge(obj);

	}

	@Override
	public Categoria buscarPorNome(String nome) {
		try {
			TypedQuery<Categoria> query = manager.createQuery("from Categoria where nome = :nome and status = true",
					Categoria.class);

			query.setParameter("nome", nome);

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	
	@Override
	public Categoria buscarPorNomeEEmpresa(String nome, Long id) {
		try {
			TypedQuery<Categoria> query = manager.createQuery("select c from Categoria c where c.nome = :nome and c.empresa.id = :idEmpresa and c.status = true",
					Categoria.class);

			query.setParameter("nome", nome);
			query.setParameter("idEmpresa", id);

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}

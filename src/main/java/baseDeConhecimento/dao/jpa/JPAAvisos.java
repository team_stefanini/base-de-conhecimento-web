package baseDeConhecimento.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoAvisos;
import baseDeConhecimento.model.Avisos;
import baseDeConhecimento.model.Usuario;

@Repository
public class JPAAvisos implements DaoAvisos {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Avisos buscar(Long id) {
		return manager.find(Avisos.class, id);
	}

	@Override
	public List<Avisos> buscarTodos() {
		TypedQuery<Avisos> query = manager.createQuery("from Avisos where status = true order by id desc", Avisos.class);
		return query.getResultList();
	}

	@Override
	public List<Avisos> buscarTodosInativos() {
		TypedQuery<Avisos> query = manager.createQuery("from Avisos where status = false", Avisos.class);
		return query.getResultList();
	}

	@Override
	@Transactional
	public void inserir(Avisos obj) {
		manager.persist(obj);

	}

	@Override
	@Transactional
	public void alterar(Avisos obj) {
		manager.merge(obj);

	}

	@Override
	public void desativar(Avisos obj) {
		obj.setStatus(false);
		manager.merge(obj);

	}
	
	@Override
	@Transactional
	public void deletar(Avisos obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	public List<Avisos> buscarAvisosPorEmpresa(List<Long> ids) {
		TypedQuery<Avisos> query = manager.createQuery("select a from Avisos a join a.empresa e where a.status = true and e.id in :ids group by a.id order by a.id desc ", Avisos.class);
		query.setParameter("ids", ids);
		return query.getResultList();
	}
	
	@Override
	public List<Usuario> buscarUsuariosEmpresa(List<Long> ids) {
		
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u join u.empresa e where u.status = true and e.id in :ids", Usuario.class);
		System.out.println(ids);
		query.setParameter("ids", ids);
		return query.getResultList();
	}

}

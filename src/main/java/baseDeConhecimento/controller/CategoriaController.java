package baseDeConhecimento.controller;

import java.net.URI;
import java.util.List;

import javax.ejb.ConcurrentAccessTimeoutException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.util.ArvoreCategoria;
import baseDeConhecimento.util.Util;

@CrossOrigin
@RestController
public class CategoriaController {
	@Autowired
	@Qualifier("JPACategoria")
	private DaoCategoria daoCategoria;

	/*
	 * METODO QUE INSERE CATEGORIA
	 */

	@RequestMapping(value = "/categoria", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Categoria> inserirCategoria(@RequestBody Categoria categoria, HttpServletRequest request) {

		categoria.setUsuario(Util.getUsuarioLogado(request));

		try {

			// Percorrendo as categorias para verificar se o nome n�o � igual
			for (Categoria categoriaIgual : daoCategoria.buscarCategoriaPorEmpresa(categoria.getEmpresa().getId())) {
				// Verificando se o nome da categoria � igual � alguma no
				// sistema
				if (categoria.getNome().trim().equals(categoriaIgual.getNome().trim())) {
					return ResponseEntity.status(600).body(null);
				}
			}

			daoCategoria.inserir(categoria);
			ArvoreCategoria.embaralhar();
			return ResponseEntity.created(URI.create("/categoria/" + categoria.getId())).body(categoria);

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Categoria>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Categoria>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * METODO QUE BUSCA TODAS AS CATEGORIAS
	 */

	@RequestMapping(value = "/categoria", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Categoria> listarCategoria() {
		return daoCategoria.buscarTodos();
	}

	/*
	 * METODO QUE BUSCA TODAS AS CATEGORIAS INATIVAS
	 */

	@RequestMapping(value = "/categoria/inativas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Categoria> listarCategoriasInativas() {
		return daoCategoria.buscarTodosInativos();
	}

	/*
	 * METODO QUE EXCLUI A CATEGORIA
	 */

	@RequestMapping(value = "categoria/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirCategoria(@PathVariable Long id) {
		daoCategoria.deletar(daoCategoria.buscar(id));
		ArvoreCategoria.embaralhar();
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE EXCLUI A CATEGORIA
	 */

	@RequestMapping(value = "categoria/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> desativarCategoria(@PathVariable Long id) {
		Categoria categoriaBuscada = daoCategoria.buscar(id);
		categoriaBuscada.setStatus(false);
		daoCategoria.desativar(categoriaBuscada);
		ArvoreCategoria.embaralhar();
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE EXCLUI A CATEGORIA
	 */

	@RequestMapping(value = "categoria/{id}", method = RequestMethod.GET)
	public ResponseEntity<Categoria> buscarCategoria(@PathVariable Long id) {

		return ResponseEntity.ok().body(daoCategoria.buscar(id));
	}

	/*
	 * METODO QUE BUSCA AS CATEGORIAS PAIS
	 */

	@RequestMapping(value = "/categoria/empresa/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Categoria> buscarCategoriaPorEmpresa(@PathVariable Long id) {
		return daoCategoria.buscarCategoriaPorEmpresa(id);
	}

	/*
	 * METODO QUE BUSCA OS PROXIMOS NIVEIS
	 */
	@RequestMapping(value = "/categoria/niveis/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Categoria> buscarNiveis(@PathVariable Long id) {

		return daoCategoria.buscarNiveis(id);
	}

	/*
	 * METODO QUE ALTERA A CATEGORIA
	 */

	@RequestMapping(value = "/categoria", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Categoria> alterarCategoria(@RequestBody Categoria categoria) {

		try {
			Categoria categoriaBuscada = daoCategoria.buscar(categoria.getId());

			try {
				if (categoriaBuscada.getCategoria().getId() != null) {
					categoria.setCategoria(categoriaBuscada.getCategoria());
				}
			} catch (NullPointerException e) {
				categoria.setCategoria(null);
			}
			daoCategoria.alterar(categoria);
			ArvoreCategoria.embaralhar();
			return ResponseEntity.created(URI.create("/categoria/" + categoria.getId())).body(categoria);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Categoria>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Categoria>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

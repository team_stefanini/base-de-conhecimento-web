package baseDeConhecimento.dao.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import baseDeConhecimento.model.QtdProcedimentoAnalista;

@Repository
public class JDBCEmpresaProcedimento {
	private Connection conexao;

	@Autowired
	public JDBCEmpresaProcedimento(DataSource dataSource) {
		try {
			conexao = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashMap<Long, Long> buscarTodos(Long id) {
		final String SQL = "SELECT procedimento_id, count(*) qtd_procedimentos FROM procedimento inner join categoria on empresa_id = "
				+ id;
		HashMap<Long, Long> mapEmpresa = new HashMap<>();

		try {

			PreparedStatement stmt = conexao.prepareStatement(SQL);

			ResultSet resultados = stmt.executeQuery();

			while (resultados.next()) {
				mapEmpresa.put(resultados.getLong("procedimento_id"), resultados.getLong("qtd_procedimentos"));
			}
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapEmpresa;
	}

	public HashMap<String, HashMap<Long, Long>> buscarQtdProcedimentosPorEmpresa(Long id_empresa) {
		final String SQL = "{call procedimentos_acessados(?)}";

		HashMap<String, HashMap<Long, Long>> mapEmpresaCompleto = new HashMap<>();
		try {

			CallableStatement stmt = (CallableStatement) conexao.prepareCall(SQL);
			stmt.setLong(1, id_empresa);
			ResultSet resultados = stmt.executeQuery();

			while (resultados.next()) {
				HashMap<Long, Long> mapEmpresa = new HashMap<>();

				mapEmpresa.put(resultados.getLong("id"), resultados.getLong("qtd"));
				mapEmpresaCompleto.put(resultados.getString("titulo"), mapEmpresa);
			}

			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			//throw new RuntimeException();
			return null;
		}
		return mapEmpresaCompleto;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> buscarQtdProcedimentosPorAnalista(Long id_empresa) {
		final String SQL = "{call procedimentos_acessados_por_analistas(?)}";
		HashMap<String, List<QtdProcedimentoAnalista>> mapEmpresa = new HashMap<>();
		try {

			CallableStatement stmt = (CallableStatement) conexao.prepareCall(SQL);
			stmt.setLong(1, id_empresa);
			ResultSet resultados = stmt.executeQuery();
			List<QtdProcedimentoAnalista> qtd = new ArrayList<>();
			while (resultados.next()) {
				QtdProcedimentoAnalista qtdProcedimento = new QtdProcedimentoAnalista();
				qtdProcedimento.setId(resultados.getLong("id_procedimento"));
				qtdProcedimento.setTitulo(resultados.getString("titulo"));
				qtdProcedimento.setQtd_acesso(resultados.getLong("qtd"));
				qtd.add(qtdProcedimento);
			}
			mapEmpresa.put("QtdProcedimentoAnalista", qtd);
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return mapEmpresa;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> buscarQtdProcedimentosPorAnalistaEmpresaCoordenador(
			List<Long> idEmpresa) {
		String SQL = "SELECT p.id as 'id_procedimento', titulo ,count(listaUsuario_id) as 'qtd' FROM basedeconhecimento.procedimento_usuario pu  inner join Procedimento p on p.id = pu.Procedimento_id inner join Usuario u on listaUsuario_id = u.id inner join categoria c on p.categoria_id = c.id where c.empresa_id in (";
		for (int i = 0; i < idEmpresa.size(); i++) {
			SQL += String.valueOf(idEmpresa.get(i));
			if (i != idEmpresa.size() - 1) {
				SQL += ",";
			}
		}

		SQL += ") and u.tipoUsuario = 2 group by Procedimento_id order by qtd desc;";

		HashMap<String, List<QtdProcedimentoAnalista>> mapEmpresa = new HashMap<>();
		try {

			PreparedStatement stmt = conexao.prepareStatement(SQL);

			ResultSet resultados = stmt.executeQuery();
			List<QtdProcedimentoAnalista> qtd = new ArrayList<>();
			while (resultados.next()) {
				QtdProcedimentoAnalista qtdProcedimento = new QtdProcedimentoAnalista();
				qtdProcedimento.setId(resultados.getLong("id_procedimento"));
				qtdProcedimento.setTitulo(resultados.getString("titulo"));
				qtdProcedimento.setQtd_acesso(resultados.getLong("qtd"));
				qtd.add(qtdProcedimento);
			}
			mapEmpresa.put("QtdProcedimentoAnalista", qtd);
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapEmpresa;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> buscarQtdProcedimentosPorAnalistaUnico(Long id) {
		final String SQL = "SELECT p.id as 'id_procedimento', p.titulo ,count(listaUsuario_id) as 'qtd' FROM basedeconhecimento.procedimento_usuario pu inner join Procedimento p on p.id = pu.Procedimento_id inner join Usuario u on listaUsuario_id = u.id inner join categoria c on p.categoria_id = c.id group by u.id, procedimento_id having(u.id) = "
				+ id + " order by qtd desc";
		HashMap<String, List<QtdProcedimentoAnalista>> mapEmpresa = new HashMap<>();
		try {

			CallableStatement stmt = (CallableStatement) conexao.prepareCall(SQL);
			ResultSet resultados = stmt.executeQuery();
			List<QtdProcedimentoAnalista> qtd = new ArrayList<>();
			while (resultados.next()) {
				QtdProcedimentoAnalista qtdProcedimento = new QtdProcedimentoAnalista();
				qtdProcedimento.setId(resultados.getLong("id_procedimento"));
				qtdProcedimento.setTitulo(resultados.getString("titulo"));
				qtdProcedimento.setQtd_acesso(resultados.getLong("qtd"));
				qtd.add(qtdProcedimento);
			}
			mapEmpresa.put("QtdProcedimentoAnalista", qtd);
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapEmpresa;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> buscarQtdProcedimentosPorAnalistaPorEmpresa(Long idEmpresa,
			Long idAnalista) {
		final String SQL = "SELECT p.id as 'id_procedimento', p.titulo ,count(listaUsuario_id) as 'qtd' FROM basedeconhecimento.procedimento_usuario pu inner join Procedimento p on p.id = pu.Procedimento_id inner join Usuario u on listaUsuario_id = u.id inner join categoria c on p.categoria_id = c.id where c.empresa_id = "
				+ idEmpresa + " and u.tipoUsuario = 2 group by u.id, Procedimento_id having(u.id) = " + idAnalista
				+ " order by qtd desc ;";
		HashMap<String, List<QtdProcedimentoAnalista>> mapEmpresa = new HashMap<>();
		try {

			CallableStatement stmt = (CallableStatement) conexao.prepareCall(SQL);
			ResultSet resultados = stmt.executeQuery();
			List<QtdProcedimentoAnalista> qtd = new ArrayList<>();
			while (resultados.next()) {
				QtdProcedimentoAnalista qtdProcedimento = new QtdProcedimentoAnalista();
				qtdProcedimento.setId(resultados.getLong("id_procedimento"));
				qtdProcedimento.setTitulo(resultados.getString("titulo"));
				qtdProcedimento.setQtd_acesso(resultados.getLong("qtd"));
				qtd.add(qtdProcedimento);
			}
			mapEmpresa.put("QtdProcedimentoAnalista", qtd);
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapEmpresa;
	}

	public HashMap<String, List<QtdProcedimentoAnalista>> buscarQtdProcedimentosPorAnalistaSistema() {
		final String SQL = "{call procedimentos_acessados_por_analistas_sistema()}";
		HashMap<String, List<QtdProcedimentoAnalista>> mapEmpresa = new HashMap<>();
		try {

			CallableStatement stmt = (CallableStatement) conexao.prepareCall(SQL);
			ResultSet resultados = stmt.executeQuery();
			List<QtdProcedimentoAnalista> qtd = new ArrayList<>();
			while (resultados.next()) {
				QtdProcedimentoAnalista qtdProcedimento = new QtdProcedimentoAnalista();
				qtdProcedimento.setId(resultados.getLong("id_procedimento"));
				qtdProcedimento.setTitulo(resultados.getString("titulo"));
				qtdProcedimento.setQtd_acesso(resultados.getLong("qtd"));
				qtd.add(qtdProcedimento);
			}
			mapEmpresa.put("QtdProcedimentoAnalista", qtd);
			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapEmpresa;
	}

	public HashMap<Long, HashMap<String, Long>> buscarQtdProcedimentosSistema() {
		final String SQL = "SELECT p.id ,p.titulo, count(listaUsuario_id) as 'qtd' FROM basedeconhecimento.procedimento_usuario pu inner join Procedimento p on p.id = pu.Procedimento_id group by Procedimento_id order by qtd desc;";

		HashMap<Long, HashMap<String, Long>> mapProcedimento = new HashMap<>();
		try {

			PreparedStatement stmt = conexao.prepareStatement(SQL);
			ResultSet resultados = stmt.executeQuery();

			while (resultados.next()) {
				HashMap<String, Long> mapProcedimentos = new HashMap<>();
				mapProcedimentos.put(resultados.getString("titulo"), resultados.getLong("qtd"));
				mapProcedimento.put(resultados.getLong("id"), mapProcedimentos);
			}

			stmt.close();
			resultados.close();

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		return mapProcedimento;
	}

}

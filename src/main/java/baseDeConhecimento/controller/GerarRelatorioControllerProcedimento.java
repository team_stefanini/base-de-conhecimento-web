package baseDeConhecimento.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.dao.jdbc.JDBCEmpresaProcedimento;
import baseDeConhecimento.dao.jpa.JPARelatorioProcedimento;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.QtdProcedimentoAnalista;
import baseDeConhecimento.model.RelatorioProcedimento;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;

@RestController
@CrossOrigin
public class GerarRelatorioControllerProcedimento {

	@Autowired
	private JDBCEmpresaProcedimento jdbcEmpresaProcedimento;

	@Autowired
	private JPARelatorioProcedimento daoRelatorioProcedimento;

	@Autowired
	@Qualifier("JPAProcedimento")
	private DaoProcedimento daoProcedimento;

	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	/*
	 * METODO QUE TRAZ OS PROCEDIMENTOS MAIS ACESSADOS POR UM ANALISTA EM
	 * ESPECIFICO
	 */
	@RequestMapping(value = "/relatorio/procedimento/{idAnalista}/analista", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<QtdProcedimentoAnalista>> procedimentosAnalista(@PathVariable Long idAnalista,
			HttpServletRequest request) {

		/*
		 * // valida se o usuario tem permiss�o para gerar relatorio try { if
		 * (!validarPermissoes(idEmpresa, request)) { return new
		 * ResponseEntity<>(HttpStatus.UNAUTHORIZED); } } catch (Exception e) {
		 * e.printStackTrace(); }
		 */

		RelatorioProcedimento relatorioProcedimento = new RelatorioProcedimento();
		relatorioProcedimento.setQtdProcedimentosAcessadosAnalista(
				jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaUnico(idAnalista));

		// Setando a data de vencimento
		Calendar cal = Calendar.getInstance();
		// Colocando o vencimento de 6 meses
		cal.add(Calendar.MONTH, 6);
		relatorioProcedimento.setDataVencimento(cal);
		relatorioProcedimento.setAnalista(daoUsuario.buscar(idAnalista));
		
		daoRelatorioProcedimento.inserir(relatorioProcedimento);

		List<QtdProcedimentoAnalista> qtdProcedimentos = new ArrayList(
				jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaUnico(idAnalista).values());

		if (qtdProcedimentos != null) {
			return ResponseEntity.ok().body(qtdProcedimentos);
		}

		return ResponseEntity.ok().body(null);
	}

	/*
	 * METODO QUE TRAZ OS PROCEDIMENTOS MAIS ACESSADOS POR UM ANALISTA EM
	 * ESPECIFICO de uma Empresa
	 */
	@RequestMapping(value = "/relatorio/procedimento/empresa/{idEmpresa}/analista/{idAnalista}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<List<QtdProcedimentoAnalista>> procedimentosAnalistaeEmpresa(@PathVariable Long idAnalista,
			@PathVariable Long idEmpresa, HttpServletRequest request) {
		/*
		 * // valida se o usuario tem permiss�o para gerar relatorio try { if
		 * (!validarPermissoes(idEmpresa, request)) { return new
		 * ResponseEntity<>(HttpStatus.UNAUTHORIZED); } } catch (Exception e) {
		 * e.printStackTrace(); }
		 */

		RelatorioProcedimento relatorioProcedimento = new RelatorioProcedimento();
		relatorioProcedimento.setQtdProcedimentosAcessadosAnalista(
				jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaPorEmpresa(idEmpresa, idAnalista));

		// Setando a data de vencimento
		Calendar cal = Calendar.getInstance();
		// Colocando o vencimento de 6 meses
		cal.add(Calendar.MONTH, 6);
		relatorioProcedimento.setDataVencimento(cal);
		relatorioProcedimento.setAnalista(daoUsuario.buscar(idAnalista));
		relatorioProcedimento.setEmpresa(daoEmpresa.buscar(idEmpresa));

		daoRelatorioProcedimento.inserir(relatorioProcedimento);

		List<QtdProcedimentoAnalista> qtdProcedimentos = new ArrayList(
				jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaPorEmpresa(idEmpresa, idAnalista).values());

		if (qtdProcedimentos != null) {
			return ResponseEntity.ok().body(qtdProcedimentos);
		}

		return ResponseEntity.ok().body(null);
	}
	
	
	public boolean validarPermissoes(Long idEmpresa, Long idAnalista, HttpServletRequest request) throws Exception {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);

		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			Empresa empresa = daoEmpresa.buscar(idEmpresa);
			List<Empresa> empresasUsuario = usuarioLogado.getEmpresa();
			
			//testa se o coordenador logado tem acesso ao analista 
			if (idAnalista != null) {
				
				List<Empresa> empresasAnalista = daoUsuario.buscar(idAnalista).getEmpresa();
				
				if (empresasUsuario.contains(empresasAnalista)) {
					return true;
				}
			}

			if (empresasUsuario.contains(empresa)) {
				return true;
			}

		} else if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return true;
		}
		return false;
	}
}

package baseDeConhecimento.dao;

import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Usuario;

public interface DaoEmpresa extends DAO<Empresa> {

	public Empresa buscarPorNome(String nome);
}

package baseDeConhecimento.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import baseDeConhecimento.model.Categoria;

@Repository
public class JDBCEmpresa {
	private Connection conexao;

	@Autowired
	public JDBCEmpresa(DataSource dataSource) {
		try {
			this.conexao = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashMap<String,HashMap<Long, Long>> buscarTodos() {
		final String SQL = "SELECT e.nome , empresa_id, count(*) qtd_usuarios  FROM usuario_empresa ue inner join Empresa e on e.id = ue.empresa_id  group by empresa_id;";
		HashMap<String,HashMap<Long, Long>> mapEmpresa = new HashMap<>();
		try {
			PreparedStatement stmt = conexao.prepareStatement(SQL);

			ResultSet resultados = stmt.executeQuery();

			while (resultados.next()) {
				HashMap<Long, Long> mapaEmpresa2 = new HashMap<>();
				mapaEmpresa2.put(resultados.getLong("empresa_id"), resultados.getLong("qtd_usuarios"));
				mapEmpresa.put(resultados.getString("nome"),mapaEmpresa2);
			}

			resultados.close();

			return mapEmpresa;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public Integer buscarTodosAnalista() {
		final String SQL = "SELECT count(*) as 'qtd' FROM basedeconhecimento.usuario where tipoUsuario = 2;";
		try {
			PreparedStatement stmt = conexao.prepareStatement(SQL);

			ResultSet resultados = stmt.executeQuery();
			int cont = 0;
			
			while(resultados.next()){
				cont += resultados.getInt("qtd");
			}
			
			resultados.close();

			return cont;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Integer buscarTodosCoordenador() {
		final String SQL = "SELECT count(*) as 'qtd' FROM basedeconhecimento.usuario where tipoUsuario = 1;";
		try {
			PreparedStatement stmt = conexao.prepareStatement(SQL);

			ResultSet resultados = stmt.executeQuery();
			int cont = 0;
			
			while(resultados.next()){
				cont += resultados.getInt("qtd");
			}
			
			
			resultados.close();

			return cont;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * METODO TRAZ AS CATEGORIAS E NIVEIS QUE N�O S�O VINCULADOS A UM
	 * PROCEDIMENTO
	 */

	public List<Categoria> buscarTodasSemProcedimento(Long idEmpresa) throws Exception {
		List<Categoria> categoriasBuscadas = new ArrayList<Categoria>();
		final String SQL = "SELECT * FROM categoria c left join procedimento p on c.id = p.categoria_id where p.categoria_id is null and c.empresa_id = ? and c.categoria_id is not null";
		PreparedStatement stmt = conexao.prepareStatement(SQL);
		stmt.setLong(1, idEmpresa);
		ResultSet resultados = stmt.executeQuery();
		while (resultados.next()) {
			Categoria categoria = new Categoria();
			categoria.setId(resultados.getLong("id"));
			categoriasBuscadas.add(categoria);
		}
		stmt.close();
		resultados.close();

		return categoriasBuscadas;
	}

	/*
	 * METODO TRAZ AS CATEGORIAS SEM EMPRESA E NIVEIS QUE N�O S�O VINCULADOS A
	 * UM PROCEDIMENTO
	 */

	public List<Categoria> buscarTodasSemProcedimentoSistema() throws Exception {
		List<Categoria> categoriasBuscadas = new ArrayList<Categoria>();
		final String SQL = "SELECT * FROM categoria c left join procedimento p on c.id = p.categoria_id where p.categoria_id is null and c.categoria_id is not null";
		PreparedStatement stmt = conexao.prepareStatement(SQL);
		ResultSet resultados = stmt.executeQuery();
		while (resultados.next()) {
			Categoria categoria = new Categoria();
			categoria.setId(resultados.getLong("id"));
			categoriasBuscadas.add(categoria);
		}
		stmt.close();
		resultados.close();

		return categoriasBuscadas;
	}

	
}

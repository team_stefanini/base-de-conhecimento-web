package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Usuario;

public interface DaoUsuario extends DAO<Usuario> {
	
	/***
	 *  Realiza o login do usu�rio e gera um token
	 * @param usuario
	 */
	public Usuario logar(Usuario usuario);
	
	public Usuario buscarPorEmail(String email);
	
	public List<Usuario> buscarUsuarioPorEmpresa(Long id);

	public List<Usuario> buscarUsuariosAnalistasPorEmpresa(Long id);

	public List<Usuario> buscarUsuariosUsuariosPorEmpresa(Long id);
	
	public List<Usuario> buscarTodosSistema();
}

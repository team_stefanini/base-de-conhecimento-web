package baseDeConhecimento.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import baseDeConhecimento.dao.DaoAvisos;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.model.Avisos;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Notification;
import baseDeConhecimento.model.PushNotification;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.Util;

@CrossOrigin
@RestController
public class AvisosController {

	@Autowired
	@Qualifier("JPAAvisos")
	private DaoAvisos daoAvisos;
	

	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	/*
	 * METODO QUE INSERE UM AVISO
	 */

	@RequestMapping(value = "/aviso", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Avisos> inserirAviso(@RequestBody Avisos aviso, HttpServletRequest request) {
		try {
			// Pegando o usuario logado
			Usuario usuarioLogado = Util.getUsuarioLogado(request);

			List<Long> empresasId = new ArrayList<>();
			List<Usuario> usuarios = new ArrayList<>();
			// Verificando se o usuario tem empresa
			if (!(usuarioLogado.getTipoUsuario().equals(TipoUsuario.ANALISTA))) {
				// Percorrendo as impresas do usuario
				for (Empresa empresa : aviso.getEmpresa()) {
					empresasId.add(empresa.getId());
				}

				for (Usuario usuario : daoAvisos.buscarUsuariosEmpresa(empresasId)) {
					if (!(usuarios.contains(usuario))) {
						usuarios.add(usuario);
					}
				}
				System.out.println("QUANTIDADE DE IDS >>>>>>>>>>>>>>>> " + empresasId.size());
				for (Usuario usuario : usuarios) {
					System.out.println("NOME USUARIO >>>>>>>>>>>>>>>>>>>>>>>>>> " + usuario.getNome());
					usuario.setNotificacao(usuario.getNotificacao() + 1);
					daoUsuario.alterar(usuario);
				}
			}

			// Setando a data de vencimento
			Calendar cal = Calendar.getInstance();
			// Colocando o vencimento de 6 meses
			cal.add(Calendar.DAY_OF_WEEK, 7);

			aviso.setVencimento(cal);

			daoAvisos.inserir(aviso);
			
			pushNotification(aviso);
			
			return ResponseEntity.created(URI.create("/aviso/" + aviso.getId())).body(aviso);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Avisos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Avisos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void pushNotification(Avisos avisos) {
		final String address = "https://fcm.googleapis.com/fcm/send";
		final String authorization = "key=AIzaSyAquU3pgsf5V7QvgtlsZHtylvaVuF5XKiA";
		final String contentType = "application/json";
		final String to = "/topics/stefanini";

		PushNotification push = new PushNotification();
		Notification notification = new Notification();
		notification.setTitle(avisos.getTitulo());
		notification.setBody(avisos.getDescricao());
		push.setTo(to);
		push.setNotification(notification);

		new Thread() {
			public void run() {
				HttpURLConnection connection = null;

				try {
					URL url = new URL(address);
					connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("POST");
					connection.setRequestProperty("Content-Type", contentType);
					connection.setRequestProperty("Authorization", authorization);
					connection.setDoOutput(true);
					DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
					// grava o json na requisi��o
					Gson gson = new Gson();
					String json = gson.toJson(push);
					wr.writeBytes(json);
					wr.close();
					// Get Response
					InputStream is = connection.getInputStream();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is));
					StringBuilder resposta = new StringBuilder();
					String line;
					while ((line = rd.readLine()) != null) {
						resposta.append(line);
						resposta.append('\r');
					}
					rd.close();
					System.out.println(resposta.toString());
					connection.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			};
		}.start();
	}

	/*
	 * METODO QUE BUSCA UM AVISO ESPECIFICO
	 */

	@RequestMapping(value = "aviso/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Avisos buscarAviso(@PathVariable Long id, HttpServletRequest request) {
		Avisos aviso = daoAvisos.buscar(id);
		if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ANALISTA)
				|| Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {
			if (aviso.isStatus()) {
				return aviso;
			} else {
				return null;
			}
		} else {
			return aviso;
		}
	}

	/*
	 * METODO QUE BUSCA TODOS OS AVISOS
	 */

	@RequestMapping(value = "/aviso", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Avisos> listarAvisos(HttpServletRequest request) {
		Date dataAtual = new Date();
		List<Avisos> avisos = daoAvisos.buscarTodos();
		for (Avisos aviso : avisos) {
			if (dataAtual.getTime() > aviso.getVencimento().getTime().getTime()) {
				daoAvisos.desativar(aviso);
			}
		}

		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		List<Long> ids = new ArrayList<>();
		if (!(usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR))) {
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				ids.add(empresa.getId());
			}

			usuarioLogado.setNotificacao(0);
			System.out.println("VOU ALTERAR O USUARIO >>>>>>>>>>>>>>>> " + ids);
			daoUsuario.alterar(usuarioLogado);
			System.out.println("ALTEREI O USUARIO >>>>>>>>>>>>>>>> " + usuarioLogado.getNotificacao());

			return daoAvisos.buscarAvisosPorEmpresa(ids);
		}

		return daoAvisos.buscarTodos();

	}

	/*
	 * METODO QUE BUSCA OS AVISOS INATIVOS NO SISTEMA
	 */

	@RequestMapping(value = "/avisos/inativos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Avisos> listarAvisosInativos() {
		return daoAvisos.buscarTodosInativos();
	}

	/*
	 * METODO QUE BUSCA OS AVISOS INATIVOS NO SISTEMA
	 */

	@RequestMapping(value = "/avisos/usuario", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Integer qtnDeAvisosUsuario(HttpServletRequest request) {
		return Util.getUsuarioLogado(request).getNotificacao();
	}

	/*
	 * METODO QUE DELETA UM AVISO
	 */

	@RequestMapping(value = "aviso/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirAviso(@PathVariable Long id) {
		daoAvisos.deletar(daoAvisos.buscar(id));
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE ALTERA UM AVISO
	 */

	@RequestMapping(value = "/aviso", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Avisos> alterarAviso(@RequestBody Avisos aviso) {
		try {
			daoAvisos.alterar(aviso);
			return ResponseEntity.created(URI.create("/aviso/" + aviso.getId())).body(aviso);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Avisos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Avisos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * METODO QUE VERIFICA QUAIS PROCEDIMENTOS N�O EST�O VALIDOS
	 */

	// @Scheduled = fixedRate fala de quanto tempo o m�todo deve ser executado
	// em millisegundos
	@Scheduled(fixedRate = 86400000)
	public void verificarProcedimentos() {
		List<Avisos> avisos = daoAvisos.buscarTodos();
		Date dataAtual = new Date();
		if (avisos.size() > 0) {
			for (Avisos aviso : avisos) {
				if (dataAtual.getTime() > aviso.getVencimento().getTime().getTime()) {
					daoAvisos.desativar(aviso);
				}
			}
		}

	}

}

package baseDeConhecimento.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.JWTVerifier;

import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.TokenJWT;

public class JWTInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		/*
		int n =1 ;
		if (n ==1) {
			return true;
		}*/
		
		if (request.getMethod().equals("OPTIONS")) {
			return true;
		}

		if (!(handler instanceof HandlerMethod)) {
			return true;
		}

		HandlerMethod methodInfo = (HandlerMethod) handler;

		
		if (methodInfo.getMethod().getName().equals("logar")
				|| methodInfo.getMethod().getName().equals("buscarPorEmail")
				|| methodInfo.getMethod().getName().equals("alterarSenha")) {
			return true;
		} else {
			String token = null;
			try {
				token = request.getHeader("Authorization");
				JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);
				Map<String, Object> claims = verifier.verify(token);

				if (claims.get("ip_usuario").equals(request.getRemoteAddr())) {
					if (verificarAcesso(methodInfo, claims)) {
						System.out.println("--interceptor -> foi validado !");
						return true;
					} else {
						response.sendError(HttpStatus.UNAUTHORIZED.value());
						System.out.println("interceptor -> n�o foi validado");
						return false;
					}
				} else {
					response.sendError(HttpStatus.UNAUTHORIZED.value());
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				if (token == null) {
					response.sendError(HttpStatus.UNAUTHORIZED.value());
				} else {
					response.sendError(HttpStatus.FORBIDDEN.value());
				}
				return false;
			}
		}
	}

	private boolean verificarAcesso(HandlerMethod methodInfo, Map<String, Object> claims) {
		System.out.println("interceptor -> TIPO USUARIO: " + claims.get("tipo_usuario"));
		if ((
		// Usuario
				methodInfo.getMethod().getName().equals("buscarUsuario")
				|| methodInfo.getMethod().getName().equals("buscarUsuariosRelacionados")
				|| methodInfo.getMethod().getName().equals("inserirUsuario")
				|| methodInfo.getMethod().getName().equals("alterarUsuario")
				|| methodInfo.getMethod().getName().equals("desativarUsuario")
				|| methodInfo.getMethod().getName().equals("listarUsuario")
				|| methodInfo.getMethod().getName().equals("alterarSenha")

				// Empresa
				|| methodInfo.getMethod().getName().equals("buscarEmpresa")
				|| methodInfo.getMethod().getName().equals("listarEmpresa")

				// categoria
				|| methodInfo.getMethod().getName().equals("listarCategoria")
				|| methodInfo.getMethod().getName().equals("buscarCategoria")
				|| methodInfo.getMethod().getName().equals("buscarCategoriaPorEmpresa")
				|| methodInfo.getMethod().getName().equals("inserirCategoria")
				|| methodInfo.getMethod().getName().equals("alterarCategoria")
				|| methodInfo.getMethod().getName().equals("desativarCategoria")
				|| methodInfo.getMethod().getName().equals("buscarCategoriaPorEmpresa")
				|| methodInfo.getMethod().getName().equals("buscarNiveis")
				
				// grupo de Solu��o
				|| methodInfo.getMethod().getName().equals("listarGrupoSolucao")
				|| methodInfo.getMethod().getName().equals("buscarGrupoSolucao")
				|| methodInfo.getMethod().getName().equals("inserirGrupoSolucao")
				|| methodInfo.getMethod().getName().equals("alterarGrupoSolucao")
				|| methodInfo.getMethod().getName().equals("desativarGrupoSolucao")
				// Procedimento
				|| methodInfo.getMethod().getName().equals("listarProcedimento")
				|| methodInfo.getMethod().getName().equals("buscarProcedimento")
				|| methodInfo.getMethod().getName().equals("inserirProcedimento")
				|| methodInfo.getMethod().getName().equals("alterarProcedimento")
				|| methodInfo.getMethod().getName().equals("desativarProcedimento")
				|| methodInfo.getMethod().getName().equals("aprovarProcedimento")
				|| methodInfo.getMethod().getName().equals("listarProcedimentoPorEmpresa")
				|| methodInfo.getMethod().getName().equals("uploadFoto")
				|| methodInfo.getMethod().getName().equals("buscarEmpresaImagens")
				|| methodInfo.getMethod().getName().equals("excluirImagemEmpresa")
				|| methodInfo.getMethod().getName().equals("buscarImagem")
				|| methodInfo.getMethod().getName().equals("listarProcedimentoInativos")
				|| methodInfo.getMethod().getName().equals("listarProcedimentoPorCategoria")
				|| methodInfo.getMethod().getName().equals("excluirProcedimento")


				
				
				// Foto
				|| methodInfo.getMethod().getName().equals("uploadFoto")
				//relatorio
				|| methodInfo.getMethod().getName().equals("gerarRelatorioPorEmpresa")
				|| methodInfo.getMethod().getName().equals("gerarRelatorioGeralEmpresa")

				//relatorio por data
				|| methodInfo.getMethod().getName().equals("buscarRelatorioPorData")
				|| methodInfo.getMethod().getName().equals("listarRelatoriosPorData")		
				|| methodInfo.getMethod().getName().equals("procedimentosAnalistaeEmpresa")
				
				//upload de categoria
				|| methodInfo.getMethod().getName().equals("uploadFileHandler")
				
				//Aviso
				|| methodInfo.getMethod().getName().equals("listarAvisos")
				|| methodInfo.getMethod().getName().equals("qtnDeAvisosUsuario")
				|| methodInfo.getMethod().getName().equals("excluirAviso")
				|| methodInfo.getMethod().getName().equals("inserirAviso"))

				
				
				

				&& claims.get("tipo_usuario").equals(TipoUsuario.COORDENADOR.toString())) {
			return true;
		}
		if (claims.get("tipo_usuario").equals(TipoUsuario.ADMINISTRADOR.toString())) {
			return true;
		}

		if (claims.get("tipo_usuario").equals(TipoUsuario.ANALISTA.toString())
				&& (methodInfo.getMethod().getName().equals("listarProcedimento")
						|| methodInfo.getMethod().getName().equals("buscarProcedimento")
						|| methodInfo.getMethod().getName().equals("listarProcedimentoPorEmpresa")
						|| methodInfo.getMethod().getName().equals("buscarPorEmail")
						|| methodInfo.getMethod().getName().equals("buscarUsuario")
						|| methodInfo.getMethod().getName().equals("alterarUsuario")
						|| methodInfo.getMethod().getName().equals("alterarSenha")
						|| methodInfo.getMethod().getName().equals("buscarCategoriaPorEmpresa")
						|| methodInfo.getMethod().getName().equals("buscarNiveis")
						|| methodInfo.getMethod().getName().equals("listarProcedimentoPorCategoria")
						|| methodInfo.getMethod().getName().equals("qtnDeAvisosUsuario")
						|| methodInfo.getMethod().getName().equals("alterarSenha")

						//Aviso
						|| methodInfo.getMethod().getName().equals("listarAvisos")
						)) {
			return true;
		}
		return false;
	}

}

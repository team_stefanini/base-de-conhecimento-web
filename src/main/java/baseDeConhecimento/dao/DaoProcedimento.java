package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Procedimento;

public interface DaoProcedimento extends DAO<Procedimento> {

	public List<Procedimento> buscarProcedimentosRelacionados(Long idCategoria, List<Long> categorias);

	public List<Procedimento> buscarProcedimentosRelacionadosPorEmpresa(List<Long> categorias);
	
	public List<Procedimento> buscarProcedimentosInativosRelacionadosPorEmpresa(List<Long> categorias);
	
	public List<Procedimento> buscarProcedimentosRelacionadosParaAnalista(Long idCategoria, List<Long> categorias);

	public List<Procedimento> buscarProcedimentosRelacionadosPorEmpresaParaAnalista(List<Long> categorias);

	public List<Procedimento> buscarTodosProcedimentosPelasCategorias(Categoria[] categorias);

	public List<Procedimento> buscarQtdProcedimentosUsuarios(Long id);

	public List<Procedimento> buscarLegiveis();

	public List<Procedimento> buscarIlegiveis();
	
	public Procedimento buscarInativos(Long id);

	public List<Procedimento> buscarTodosEmpresaCoordenador(List<Long> idEmpresa);
	
	public Procedimento buscarProcedimentoPorTitulo(Long idEmpresa, String titulo);

	List<Procedimento> buscarProcedimentosEmpresaUsuarioLogado(List<Long> idEmpresas);
}

package baseDeConhecimento.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoGrupoSolucao;
import baseDeConhecimento.model.GrupoSolucao;

@Repository
public class JPAGrupoSolucao implements DaoGrupoSolucao {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public GrupoSolucao buscar(Long id) {
		return manager.find(GrupoSolucao.class, id);
	}

	@Override
	public List<GrupoSolucao> buscarTodos() {
		TypedQuery<GrupoSolucao> query = manager.createQuery("from GrupoSolucao where status = true",
				GrupoSolucao.class);

		return query.getResultList();
	}

	@Override
	public List<GrupoSolucao> buscarTodosInativos() {
		TypedQuery<GrupoSolucao> query = manager.createQuery("from GrupoSolucao where status = false",
				GrupoSolucao.class);

		return query.getResultList();
	}

	@Transactional
	@Override
	public void inserir(GrupoSolucao obj) {
		manager.persist(obj);
	}

	@Transactional
	@Override
	public void alterar(GrupoSolucao obj) {
		manager.merge(obj);
	}

	@Transactional
	@Override
	public void deletar(GrupoSolucao obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	@Transactional
	public void desativar(GrupoSolucao obj) {
		obj.setStatus(false);
		manager.merge(obj);
	}

	@Override
	public GrupoSolucao buscarPorNome(String nome) {
		try {
			TypedQuery<GrupoSolucao> query = manager
					.createQuery("from GrupoSolucao where nome = :nome and status = true", GrupoSolucao.class);

			query.setParameter("nome", nome);

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}

package baseDeConhecimento.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.util.Util;

@RestController
@CrossOrigin
public class ExcelController {

	@Autowired
	private ServletContext servletContext;

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	@Autowired
	@Qualifier("JPACategoria")
	private DaoCategoria daoCategoria;

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public ResponseEntity<Void> uploadFileHandler(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		
		System.out.println("=========================================================================================");
		System.out.println("Exel controller error code 1952 - ta chegando no excel controller");
		System.out.println("=========================================================================================");


		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();

				String caminhoDiretorio = servletContext.getRealPath("/");
				String caminhoExcel = "/arqExcel/teste" + ".csv";

				// Create the file on server
				File serverFile = new File(caminhoDiretorio + caminhoExcel);

				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				BufferedReader br = new BufferedReader(new FileReader(serverFile));

				String line = "";
				int ignorar = 1;
				while ((line = br.readLine()) != null) {
					if (ignorar == 1) {
						ignorar++;
					} else {

						// "," ou ";" de acordo com o arquivo
						String[] row = line.split(";");

						if (row[0].isEmpty()) {
							continue;
						}
	
						Empresa empresa = daoEmpresa.buscarPorNome(row[0]);
																		
						// Verifica se a empresa existe no sistema
						if (empresa == null) {
							empresa = new Empresa();

							empresa.setNome(row[0]);
							daoEmpresa.inserir(empresa);
						}
						int i = 0;
						// Passa pelas linhas da tabela
						for (String str : row) {
							if (i == 0) {
								i++;
								continue;
							}
							// Verifica se a linha tem algum conteudo
							if (!str.isEmpty()) {
								Categoria categoria = daoCategoria.buscarPorNomeEEmpresa(str, empresa.getId());

								// Verifica se a categoria j� existe, se n�o
								// existir ele cria uma nova
								if (categoria == null) {
									categoria = new Categoria();
									categoria.setNome(str);
									categoria.setEmpresa(empresa);
									categoria.setUsuario(Util.getUsuarioLogado(request));

									// Verifica se � um nivel
									if (i > 1) {
										System.out.println("ANTES DE SETAR O NIVEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + row[i - 1]);
										categoria.setCategoria(daoCategoria.buscarPorNomeEEmpresa(row[i - 1],empresa.getId()));
										System.out.println("DEPOIS DE SETAR O NIVEL >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + daoCategoria.buscarPorNome(row[i - 1]));
									}
									daoCategoria.inserir(categoria);

								}
							}
							if (++i == 6) {
								break;
							}
						}
					}
				}
				serverFile.delete();
				return new ResponseEntity<Void>(HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
	}
}

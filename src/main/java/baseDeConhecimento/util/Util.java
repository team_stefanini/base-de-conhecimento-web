package baseDeConhecimento.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWTVerifier;

import baseDeConhecimento.controller.ProcedimentoController;
import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.dao.jdbc.JDBCEmpresa;
import baseDeConhecimento.dao.jdbc.JDBCEmpresaProcedimento;
import baseDeConhecimento.dao.jpa.JPARelatorioEmpresa;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Procedimento;
import baseDeConhecimento.model.RelatorioEmpresa;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.TokenJWT;
import baseDeConhecimento.model.Usuario;

@Component
public class Util {
	
	@Autowired
	@Qualifier("JPACategoria")
	private static DaoCategoria daoCategoria;
	
	@Autowired
	@Qualifier("JPAProcedimento")
	private static DaoProcedimento daoProcedimento;
	
	@Autowired
	private static JDBCEmpresaProcedimento jdbcEmpresaProcedimento;
	
	@Autowired
	@Qualifier("JPAEmpresa")
	private static DaoEmpresa daoEmpresa;
	
	@Autowired
	private static JDBCEmpresa jdbcEmpresa;
	
	@Autowired
	private static JPARelatorioEmpresa daoRelatorioEmpresa;
	
	private static DaoUsuario daoUsuario;
	
	@Autowired
	public Util (DaoUsuario dao){
		daoUsuario = dao;
	}
	
	//Devolve o Usuario que esta logado
	public static Usuario getUsuarioLogado(HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			JWTVerifier verifier = new JWTVerifier(TokenJWT.SECRET);
			Map<String, Object> claims = verifier.verify(token);

			String idString = claims.get("id_usuario").toString();

			Long id = Long.parseLong(idString);
						
			return daoUsuario.buscar(id);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}	
	
		/**
		 * Devolve os ids da  empresa do Usuario que esta logado
		 * @param request
		 * @return
		 */
		public static List<Long> getEmpresasUsuarioLogado(Usuario usuarioLogado) {
			try {
				List<Empresa> empresas = usuarioLogado.getEmpresa();
				List<Long> idEmpresas = new ArrayList<>();
				for (Empresa empresa : empresas) {
					
					idEmpresas.add(empresa.getId());
					
				}
				
				return idEmpresas;

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

		}
	
	/***
	 * Recebe uma String e retorna um Calendar
	 * @param dataString
	 * @return
	 */
	public static Calendar StringParaCalendar(String dataString){
		//tranforma a data em calendar 
				DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				Calendar c = Calendar.getInstance();
				try {
					 c.setTime(df.parse(dataString));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return c;
		}
	
	
	public static String encryptPassword(String senha){
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		String md5 = encoder.encodePassword(senha, null);
		return md5;
	}
	
}

package baseDeConhecimento.util;

import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.model.Categoria;

@Component
public class ArvoreCategoria {
	// @Autowired
	// @Qualifier("JPACategoria")
	private static DaoCategoria daoCategoria;

	public static HashMap<Long, String> mapaCategorias = new HashMap<>();
	public static HashMap<Long, String> mapaCatNomes = new HashMap<>();

	@Autowired
	public ArvoreCategoria(DaoCategoria pDaoCategoria) {
		daoCategoria = pDaoCategoria;
		embaralhar();
	}

	public static String[] getFilhos(Long idCategoria) {
		try {
			String[] filhos = mapaCategorias.get(idCategoria).split(";");
			System.out.println("CLASSE ARVORE GET FILHOS: " + filhos.length);
			return filhos;
		} catch (Exception e) {
			return new String[] {};
		}
	}

	public static void embaralhar() {
		System.out.println("ENTROU");
		mapaCategorias.clear();
		List<Categoria> categorias = daoCategoria.buscarTodos();
		for (Categoria c : categorias) {
			String nome = c.getCategoria() == null ? "0;" + c.getNome() : c.getCategoria().getId() + ";" + c.getNome();
			mapaCatNomes.put(c.getId(), nome);
			categorias.stream().filter(cat -> cat.getCategoria() != null && cat.getCategoria().getId() == c.getId())
					.forEach(new Consumer<Categoria>() {
						public void accept(Categoria t) {

							if (!mapaCategorias.containsKey(c.getId())) {
								mapaCategorias.put(c.getId(), t.getId().toString());
							} else {
								String filhos = mapaCategorias.get(c.getId());
								filhos += ";" + t.getId();
								mapaCategorias.put(c.getId(), filhos);
							}
						};
					});

		}		
	}

	public static String getCaminho(Long idCategoria) {
		String caminho; 
		String caminhoAbsoluto = "";
		String valor = mapaCatNomes.get(idCategoria);
		String dados[] = valor.split(";");
		Long pai = Long.parseLong(dados[0]);
		caminho = dados[1];
		Integer contador = 1;
		while (pai != 0) {
			valor = mapaCatNomes.get(pai);
			if (valor != null) {
				dados = valor.split(";");
				pai = Long.parseLong(dados[0]);
				caminho = dados[1] + " > " + caminho;	
				System.out.println("ARVORE >>>>>>>>>>>>>>>>> " + dados[1]);
				if (contador%4 == 0) {
					caminhoAbsoluto = caminho;
				}
				contador++;
				
			}
		}

		return caminhoAbsoluto.toString();
	}
}

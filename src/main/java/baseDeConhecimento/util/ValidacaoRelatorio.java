package baseDeConhecimento.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import baseDeConhecimento.controller.ProcedimentoController;
import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.dao.jdbc.JDBCEmpresa;
import baseDeConhecimento.dao.jdbc.JDBCEmpresaProcedimento;
import baseDeConhecimento.dao.jpa.JPARelatorioEmpresa;
import baseDeConhecimento.dao.jpa.JPARelatorioProcedimento;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Procedimento;
import baseDeConhecimento.model.QtdProcedimentoAnalista;
import baseDeConhecimento.model.RelatorioEmpresa;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;

@Component
public class ValidacaoRelatorio {

	@Autowired
	@Qualifier("JPAUsuario")
	private static DaoUsuario daoUsuario;

	@Autowired
	private static JDBCEmpresa jdbcEmpresa;

	@Autowired
	private static JDBCEmpresaProcedimento jdbcEmpresaProcedimento;

	@Autowired
	@Qualifier("JPAEmpresa")
	private static DaoEmpresa daoEmpresa;

	@Autowired
	@Qualifier("JPACategoria")
	private static DaoCategoria daoCategoria;

	@Autowired
	@Qualifier("JPAProcedimento")
	private static DaoProcedimento daoProcedimento;

	@Autowired
	private static JPARelatorioEmpresa daoRelatorioEmpresa;

	@Autowired
	private JPARelatorioProcedimento daoRelatorioProcedimento;

	@Autowired
	public ValidacaoRelatorio(JDBCEmpresa jdbcEmpresa, DaoEmpresa daoEmpresa, DaoCategoria daoCategoria,
			DaoUsuario daoUsuario, DaoProcedimento daoProcedimento, JDBCEmpresaProcedimento jdbcEmpresaProcedimento,
			JPARelatorioEmpresa jpaRelatorioEmpresa) {
		this.jdbcEmpresa = jdbcEmpresa;
		this.daoEmpresa = daoEmpresa;
		this.daoCategoria = daoCategoria;
		this.daoUsuario = daoUsuario;
		this.daoProcedimento = daoProcedimento;
		this.jdbcEmpresaProcedimento = jdbcEmpresaProcedimento;
		this.daoRelatorioEmpresa = jpaRelatorioEmpresa;

		System.out.println("CONSTRUTOR DA CLASSE DE VALIDA��O DAO USUARIO >>>>>>>>>>>>>>>>>> "
				+ daoUsuario.buscarTodosSistema().size());
	}

	/*
	 * METODO VERIFICA QUAL USUARIO �, E SETA A QUANTIDADE DE CATEGORIAS PAI QUE
	 * ELE PODE VER
	 */
	public static RelatorioEmpresa verificarUsuarioCategoriasPai(boolean alterar, Usuario usuarioLogado,
			RelatorioEmpresa relatorioDoMes) {
		RelatorioEmpresa relatorioEmpresa = new RelatorioEmpresa();

		// Verifica qual o metodo que foi chamado -> alterar, inserir
		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			List<Categoria> categoriasPai = new ArrayList<Categoria>();

			// Pega s� o hash das empresas que o coordenador est� vinculado
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				categoriasPai.addAll(daoCategoria.buscarCategoriaPorEmpresa(empresa.getId()));
			}
			if (alterar) {

				relatorioDoMes.setQntCategoriasPai(categoriasPai.size());

				return relatorioDoMes;
			} else {

				relatorioEmpresa.setQntCategoriasPai(categoriasPai.size());
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		} else {
			if (alterar) {

				relatorioDoMes.setQntCategoriasPai(daoCategoria.buscarCategoriasPai().size());

				return relatorioDoMes;
			} else {
				relatorioEmpresa.setQntCategoriasPai(daoCategoria.buscarCategoriasPai().size());
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		}
	}

	/*
	 * METODO VERIFICA QUAL USUARIO �, E SETA A QUANTIDADE DE CATEGORIAS PAI QUE
	 * ELE PODE VER
	 */
	public static RelatorioEmpresa verificarRelatorioGeralUsuario(boolean alterar, Usuario usuarioLogado,
			RelatorioEmpresa relatorioDoMes) {
		RelatorioEmpresa relatorioEmpresa = new RelatorioEmpresa();

		// Verifica qual o metodo que foi chamado -> alterar, inserir
		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			List<Usuario> usuariosAnalistas = new ArrayList<Usuario>();

			// Pega s� o hash das empresas que o coordenador est� vinculado
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				usuariosAnalistas.addAll((daoUsuario.buscarUsuariosAnalistasPorEmpresa(empresa.getId())));
			}
			if (alterar) {

				relatorioDoMes.setQntAnalistas(usuariosAnalistas.size());
				relatorioDoMes.setQntUsuarios(daoUsuario.buscarTodosSistema().size());

				return relatorioDoMes;
			} else {

				relatorioEmpresa.setQntUsuarios(daoUsuario.buscarTodosSistema().size());
				relatorioEmpresa.setQntAnalistas(usuariosAnalistas.size());
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		} else {
			if (alterar) {

				relatorioDoMes.setQntUsuarios(daoUsuario.buscarTodosSistema().size());
				relatorioDoMes.setQntAnalistas(jdbcEmpresa.buscarTodosAnalista());
				relatorioDoMes.setQntCoodenadores(jdbcEmpresa.buscarTodosCoordenador());

				return relatorioDoMes;
			} else {
				relatorioEmpresa.setQntUsuarios(daoUsuario.buscarTodosSistema().size());
				relatorioEmpresa.setQntAnalistas(jdbcEmpresa.buscarTodosAnalista());
				relatorioEmpresa.setQntCoodenadores(jdbcEmpresa.buscarTodosCoordenador());
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		}
	}

	/*
	 * METODO VERIFICA QUAL USUARIO �, E SETA AS RESPECTIVAS INFORMA��ES
	 */
	public static RelatorioEmpresa verificarUsuarioEmpresa(boolean alterar, Usuario usuarioLogado,
			RelatorioEmpresa relatorioDoMes) {
		RelatorioEmpresa relatorioEmpresa = new RelatorioEmpresa();

		// Verifica qual o metodo que foi chamado -> alterar, inserir
		if (usuarioLogado.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			HashMap<String, HashMap<Long, Long>> mapaEmpresa = jdbcEmpresa.buscarTodos();
			HashMap<String, HashMap<Long, Long>> mapaEmpresaUsuario = new HashMap<>();
			List<Categoria> categoriasPai = new ArrayList<Categoria>();
			List<Usuario> usuariosAnalistas = new ArrayList<Usuario>();
			List<Usuario> usuariosPorEmpresa = new ArrayList<Usuario>();
			List<Long> idsEmpresas = new ArrayList<>();

			// Pega s� o hash das empresas que o coordenador est� vinculado
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				mapaEmpresaUsuario.put(empresa.getNome(), mapaEmpresa.get(empresa.getNome()));
				categoriasPai.addAll(daoCategoria.buscarCategoriaPorEmpresa(empresa.getId()));
				usuariosAnalistas.addAll((daoUsuario.buscarUsuariosAnalistasPorEmpresa(empresa.getId())));
				usuariosPorEmpresa.addAll((daoUsuario.buscarUsuariosUsuariosPorEmpresa(empresa.getId())));
				idsEmpresas.add(empresa.getId());
			}
			if (alterar) {

				relatorioDoMes.setQntEmpresas(usuarioLogado.getEmpresa().size());

				relatorioDoMes.setQntUsuariosPorEmpresa(mapaEmpresaUsuario);

				relatorioDoMes.setQntAnalistas(usuariosAnalistas.size());

				relatorioDoMes.setQntUsuarios(usuariosPorEmpresa.size());

				relatorioDoMes.setQntCategoriasPai(categoriasPai.size());

				relatorioDoMes.setQtdProcedimentosAcessadosAnalista(
						jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaEmpresaCoordenador(idsEmpresas));

				relatorioDoMes
						.setQtdProcedimentosSistema(daoProcedimento.buscarTodosEmpresaCoordenador(idsEmpresas).size());

				return relatorioDoMes;
			} else {

				// Seta a quantidade de empresas que o usuario contem
				relatorioEmpresa.setQntEmpresas(usuarioLogado.getEmpresa().size());

				// Seta a quantidade de usuarios as empresas contem
				relatorioEmpresa.setQntUsuariosPorEmpresa(mapaEmpresaUsuario);

				// Seta a quantidade de categorias pai as empresas do usuario
				// contem
				relatorioEmpresa.setQntCategoriasPai(categoriasPai.size());

				relatorioEmpresa.setQntAnalistas(usuariosAnalistas.size());

				relatorioEmpresa.setQntUsuarios(usuariosPorEmpresa.size());

				relatorioEmpresa.setQtdProcedimentosAcessadosAnalista(
						jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaEmpresaCoordenador(idsEmpresas));

				relatorioEmpresa
						.setQtdProcedimentosSistema(daoProcedimento.buscarTodosEmpresaCoordenador(idsEmpresas).size());

				// Seta o usuario que gerou o relatorio
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		} else {
			if (alterar) {

				// Seta a quantidade de empresas que o usuario contem
				relatorioDoMes.setQntEmpresas(daoEmpresa.buscarTodos().size());

				// Seta a quantidade de categorias pai as empresas do usuario
				// contem
				relatorioDoMes.setQntUsuariosPorEmpresa(jdbcEmpresa.buscarTodos());

				relatorioDoMes.setQntUsuarios(daoUsuario.buscarTodosSistema().size());

				relatorioDoMes.setQntAnalistas(jdbcEmpresa.buscarTodosAnalista());

				relatorioDoMes.setQntCoodenadores(jdbcEmpresa.buscarTodosCoordenador());

				relatorioDoMes.setQntCategoriasPai(daoCategoria.buscarCategoriasPai().size());

				relatorioDoMes.setQtdProcedimentosAcessadosAnalista(
						jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaSistema());

				relatorioDoMes.setQtdProcedimentosSistema(daoProcedimento.buscarTodos().size());

				return relatorioDoMes;
			} else {

				// Seta a quantidade de empresas que o usuario contem
				relatorioEmpresa.setQntEmpresas(daoEmpresa.buscarTodos().size());

				relatorioEmpresa.setQntUsuariosPorEmpresa(jdbcEmpresa.buscarTodos());

				relatorioEmpresa.setQntCategoriasPai(daoCategoria.buscarCategoriasPai().size());

				relatorioEmpresa.setQntUsuarios(daoUsuario.buscarTodosSistema().size());

				relatorioEmpresa.setQntAnalistas(jdbcEmpresa.buscarTodosAnalista());

				relatorioEmpresa.setQntCoodenadores(jdbcEmpresa.buscarTodosCoordenador());

				relatorioEmpresa.setQtdProcedimentosAcessadosAnalista(
						jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalistaSistema());

				relatorioEmpresa.setQtdProcedimentosSistema(daoProcedimento.buscarTodos().size());

				// Seta o usuario que gerou o relatorio
				relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);

				return relatorioEmpresa;
			}

		}
	}

	public static RelatorioEmpresa gerarRelatorioEmpresa(Long idEmpresa, Usuario usuarioLogado) {

		Calendar mes = Calendar.getInstance();
		mes.add(Calendar.MONTH, 1);
		// busca um relatorio por mes e testa se vai inserir ou alterar o
		// relatorio
		RelatorioEmpresa relatorioBuscado = daoRelatorioEmpresa.buscarRelatorioDoMes(usuarioLogado.getId(), idEmpresa,
				mes);
		if (relatorioBuscado != null) {
			// seta a quantidades de categorias PAI de uma empresa
			relatorioBuscado.setQntCategoriasPai(daoCategoria.buscarCategoriaPorEmpresa(idEmpresa).size());

			// seta quantos usuarios tem na empresa e quantos s�o analistas e
			// quantos coordenadores
			List<Usuario> usuariosEmpresa = daoUsuario.buscarUsuarioPorEmpresa(idEmpresa);

			Integer qtdAnalista = 0;
			Integer qtdCoordenador = 0;

			if (usuariosEmpresa != null) {
				for (Usuario usuario : usuariosEmpresa) {

					if (usuario.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {
						qtdCoordenador += 1;
					} else {
						qtdAnalista += 1;
					}
				}

				relatorioBuscado.setQntUsuarios(usuariosEmpresa.size());
				relatorioBuscado.setQntAnalistas(qtdAnalista);
				relatorioBuscado.setQntCoodenadores(qtdCoordenador);
			}

			// seta a quantidade de procedimento da Empresa

			List<Categoria> categorias = daoCategoria.buscarCategoriaPorEmpresa(idEmpresa);

			List<Long> categoriasId = new ArrayList<>();

			for (Categoria categoria : categorias) {
				String[] catFilhos = ArvoreCategoria.getFilhos(categoria.getId());
				ProcedimentoController.populaLista(catFilhos, categoriasId);
			}

			List<Procedimento> procedimentos = daoProcedimento.buscarProcedimentosRelacionadosPorEmpresa(categoriasId);
			if (procedimentos == null) {
				relatorioBuscado.setQntProcedimentos(0);
			} else {
				relatorioBuscado.setQntProcedimentos(procedimentos.size());
			}

			// seta os procedimentos mais acessados de uma empresa
			HashMap<String, HashMap<Long, Long>> mapaProcedimentos = jdbcEmpresaProcedimento
					.buscarQtdProcedimentosPorEmpresa(idEmpresa);
			relatorioBuscado.setQtdProcedimentosAcessados(mapaProcedimentos);

			// seta o usuario que gerou o relatorio
			relatorioBuscado.setUsuarioGerouRelatorio(usuarioLogado);
			// seta a empresa do relatorio
			relatorioBuscado.setEmpresa(daoEmpresa.buscar(idEmpresa));

			// seta os procedimetos acessados por analista
			relatorioBuscado.setQtdProcedimentosAcessadosAnalista(
					jdbcEmpresaProcedimento.buscarQtdProcedimentosPorAnalista(idEmpresa));

			// seta as categoias sem procedimentos de uma empresa
			List<String> caminhos = new ArrayList<>();

			try {
				List<Categoria> listaId = jdbcEmpresa.buscarTodasSemProcedimento(idEmpresa);
				for (Categoria categoria : listaId) {
					if (!(ArvoreCategoria.getCaminho(categoria.getId()) == "")) {
						caminhos.add(ArvoreCategoria.getCaminho(categoria.getId()));
					}
				}

				relatorioBuscado.setCaminhoCategoriasSemProcedimento(caminhos);

			} catch (Exception e) {
				e.printStackTrace();
			}

			daoRelatorioEmpresa.alterar(relatorioBuscado);

			return relatorioBuscado;

		} else {

			RelatorioEmpresa relatorioEmpresa = new RelatorioEmpresa();

			// seta a quantidades de categorias PAI de uma empresa
			relatorioEmpresa.setQntCategoriasPai(daoCategoria.buscarCategoriaPorEmpresa(idEmpresa).size());

			// seta quantos usuarios tem na empresa e quantos s�o analistas e
			// quantos coordenadores
			List<Usuario> usuariosEmpresa = daoUsuario.buscarUsuarioPorEmpresa(idEmpresa);

			Integer qtdAnalista = 0;
			Integer qtdCoordenador = 0;

			if (usuariosEmpresa != null) {
				for (Usuario usuario : usuariosEmpresa) {

					if (usuario.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {
						qtdCoordenador += 1;
					} else {
						qtdAnalista += 1;
					}
				}

				relatorioEmpresa.setQntUsuarios(usuariosEmpresa.size());
				relatorioEmpresa.setQntAnalistas(qtdAnalista);
				relatorioEmpresa.setQntCoodenadores(qtdCoordenador);
			}

			// seta a quantidade de procedimento da Empresa

			List<Categoria> categorias = daoCategoria.buscarCategoriaPorEmpresa(idEmpresa);

			List<Long> categoriasId = new ArrayList<>();

			for (Categoria categoria : categorias) {
				String[] catFilhos = ArvoreCategoria.getFilhos(categoria.getId());
				ProcedimentoController.populaLista(catFilhos, categoriasId);
			}

			List<Procedimento> procedimentos = daoProcedimento.buscarProcedimentosRelacionadosPorEmpresa(categoriasId);
			if (procedimentos == null) {
				relatorioEmpresa.setQntProcedimentos(0);
			} else {
				relatorioEmpresa.setQntProcedimentos(procedimentos.size());
			}
			// seta os procedimentos mais acessados de uma empresa
			HashMap<String, HashMap<Long, Long>> mapaProcedimentos = jdbcEmpresaProcedimento
					.buscarQtdProcedimentosPorEmpresa(idEmpresa);
			relatorioEmpresa.setQtdProcedimentosAcessados(mapaProcedimentos);

			// seta o usuario que gerou o relatorio
			relatorioEmpresa.setUsuarioGerouRelatorio(usuarioLogado);
			// seta a empresa do relatorio
			relatorioEmpresa.setEmpresa(daoEmpresa.buscar(idEmpresa));

			// seta os procedimetos acessados por analista

			HashMap<String, List<QtdProcedimentoAnalista>> mapAcessoAnalista = jdbcEmpresaProcedimento
					.buscarQtdProcedimentosPorAnalista(idEmpresa);

			if (!mapAcessoAnalista.isEmpty()) {
				relatorioEmpresa.setQtdProcedimentosAcessadosAnalista(mapAcessoAnalista);
			} else {
				relatorioEmpresa
						.setQtdProcedimentosAcessadosAnalista(new HashMap<String, List<QtdProcedimentoAnalista>>());
			}

			// seta as categoias sem procedimentos de uma empresa
			List<String> caminhos = new ArrayList<>();
			try {
				List<Categoria> listaId = jdbcEmpresa.buscarTodasSemProcedimento(idEmpresa);
				for (Categoria categoria : listaId) {
					caminhos.add(ArvoreCategoria.getCaminho(categoria.getId()));
				}

				relatorioEmpresa.setCaminhoCategoriasSemProcedimento(caminhos);
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, 6);
				relatorioEmpresa.setDataVencimento(cal);

			} catch (Exception e) {
				e.printStackTrace();
			}

			daoRelatorioEmpresa.inserir(relatorioEmpresa);

			return relatorioEmpresa;
		}

	}
}

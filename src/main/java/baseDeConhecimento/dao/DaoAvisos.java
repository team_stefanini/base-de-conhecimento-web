package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Avisos;
import baseDeConhecimento.model.Usuario;

public interface DaoAvisos extends DAO<Avisos>{
	public List<Avisos> buscarAvisosPorEmpresa(List<Long> ids);
	public List<Usuario> buscarUsuariosEmpresa(List<Long> ids);
}

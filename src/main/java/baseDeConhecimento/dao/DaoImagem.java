package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Imagem;

public interface DaoImagem extends DAO<Imagem> {
	public void deletarImagens(List<Long> idImagens);

	public List<Imagem> buscarImagensEmpresa(Long id);
}

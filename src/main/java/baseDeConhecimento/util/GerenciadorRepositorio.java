package baseDeConhecimento.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component("upload")
public class GerenciadorRepositorio {

	private static File pastaDoRepositorio = null;
	public String caminho = null;

	public void iniciarRepositorio(ServletContext servetContext) {

		if (servetContext.getClass().getName().equals("Empresa")) {
			final File file = new File(servetContext.getRealPath("/"));

			this.caminho = file.getParentFile().getAbsolutePath() + "\\Empresa\\imagens\\";

			pastaDoRepositorio = new File(caminho);

			if (!pastaDoRepositorio.exists()) {
				pastaDoRepositorio.mkdirs();
			}
			System.out.println(caminho);
		} else {
			final File file = new File(servetContext.getRealPath("/"));

			this.caminho = file.getParentFile().getAbsolutePath() + "\\Procedimento\\imagens\\";

			pastaDoRepositorio = new File(caminho);

			if (!pastaDoRepositorio.exists()) {
				pastaDoRepositorio.mkdirs();
			}
			System.out.println(caminho);
		}

	}

	public static File getPastaDoRepositorio() {
		return pastaDoRepositorio;
	}

	public static void inserirImagem(ByteArrayInputStream file, String nomeDoArquivo, String tipo) {

		try {
			System.out.println("SALVANDO IMAGEM");
			BufferedImage imagem = ImageIO.read(file);
			// File arquivoParaSalvar = new
			// File(pastaDoRepositorio.getAbsolutePath() + "\\" + tipo + "\\" +
			// nomeDoArquivo + ".jpg");
			File arquivoParaSalvar = null;
			if (tipo.equals("empresa")) {
				arquivoParaSalvar = new File("C:\\Empresa\\" + "\\" + nomeDoArquivo + ".jpg");
			} else {
				arquivoParaSalvar = new File("C:\\Procedimento\\" + "\\" + nomeDoArquivo + ".jpg");
			}

			arquivoParaSalvar.createNewFile();

			System.out.println("Arquivo para salvar: " + arquivoParaSalvar.getAbsolutePath());

			ImageIO.write(imagem, "jpg", arquivoParaSalvar);
		} catch (IOException e) {
			System.out.println("Erro tentando salvar a imagem");
			e.printStackTrace();
		}

	}
}

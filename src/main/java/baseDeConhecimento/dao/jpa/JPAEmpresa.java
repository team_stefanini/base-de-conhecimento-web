package baseDeConhecimento.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.model.Empresa;

@Repository
public class JPAEmpresa implements DaoEmpresa {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Empresa buscar(Long id) {
		return manager.find(Empresa.class, id);
	}

	@Override
	public List<Empresa> buscarTodos() {
		TypedQuery<Empresa> query = manager.createQuery("from Empresa where status = true order by nome", Empresa.class);

		return query.getResultList();
	}

	@Override
	public List<Empresa> buscarTodosInativos() {
		TypedQuery<Empresa> query = manager.createQuery("from Empresa where status = false", Empresa.class);

		return query.getResultList();
	}

	public List<Empresa> buscarDeUsuario(Long id) {
		TypedQuery<Empresa> query = manager.createQuery("from Empresa where id = :id and status = true", Empresa.class);

		query.setParameter("id", id);

		return query.getResultList();
	}

	@Override
	@Transactional
	public void inserir(Empresa obj) {
		manager.persist(obj);
	}

	@Override
	@Transactional
	public void alterar(Empresa obj) {
		manager.merge(obj);

	}

	@Override
	@Transactional
	public void deletar(Empresa obj) {
		manager.remove(buscar(obj.getId()));
	}

	@Override
	@Transactional
	public void desativar(Empresa obj) {
		obj.setStatus(false);
		manager.merge(obj);
	}

	@Override
	public Empresa buscarPorNome(String nome) {
		try {
			TypedQuery<Empresa> query = manager.createQuery("from Empresa where nome = :nome",
					Empresa.class);

			query.setParameter("nome", nome);

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}

package baseDeConhecimento.dao;

import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.GrupoSolucao;

public interface DaoGrupoSolucao extends DAO<GrupoSolucao> {


	public GrupoSolucao buscarPorNome(String nome);
}

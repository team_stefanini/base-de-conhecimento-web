package baseDeConhecimento.dao.jpa;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import baseDeConhecimento.model.RelatorioProcedimento;
import baseDeConhecimento.model.Usuario;

@Repository
public class JPARelatorioProcedimento {

	@PersistenceContext
	private EntityManager manager;

	public RelatorioProcedimento buscar(Long id) {
		return manager.find(RelatorioProcedimento.class, id);
	}

	public List<RelatorioProcedimento> buscarTodos() {
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where status = true",
				RelatorioProcedimento.class);

		return query.getResultList();
	}

	public List<RelatorioProcedimento> buscarTodosInativos() {
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where status = false",
				RelatorioProcedimento.class);

		return query.getResultList();
	}

	@Transactional
	public void inserir(RelatorioProcedimento obj) {
		manager.persist(obj);

	}

	@Transactional
	public void alterar(RelatorioProcedimento obj) {
		// TODO Auto-generated method stub

	}

	@Transactional
	public void deletar(RelatorioProcedimento obj) {
		// TODO Auto-generated method stub

	}

	@Transactional
	public void desativar(RelatorioProcedimento obj) {
		// TODO Auto-generated method stub

	}
	
	public RelatorioProcedimento buscarReltorioGeralEmpresa(Calendar cal, Usuario usuarioLogado){
		try {
			TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and 		 is not null", RelatorioProcedimento.class);
			query.setParameter("mes",cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	//come�a as busca por data 
	/**
	 * Tr�z todos os relatorios geral de Procedimento(objeto) por uma data especifica
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
	public List<RelatorioProcedimento> RelatorioGeralProcedimentoPorData(Calendar dataInicial, Calendar dataFinal){
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where dataEmissao >= :dataInicial and dataEmissao <= :dataFinal and qtdProcedimentosSistema is not null", RelatorioProcedimento.class);
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		return query.getResultList();
	}
	
	/**
	 * Tr�z todos os relatorios dos procedimento mais acessados por analista por um intervalo de tempo
	 * @param dataInicial
	 * @param dataFinal
	 * @param idEmpresa
	 * @return
	 */
	public List<RelatorioProcedimento> relatorioProcedimentoMaisAcessadoAnalistaData(Calendar dataInicial, Calendar dataFinal, Long idEmpresa){
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where dataEmissao >= :dataInicial and dataEmissao <= :dataFinal and empresa_id = :idEmpresa and QtdProcedimentosAcessadosAnalista is not null", RelatorioProcedimento.class);
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("idEmpresa", idEmpresa);
		return query.getResultList();
	}
	
	
	/**
	 * tr�s todos os relatorios de procedimentos acessados por um analista especifico do sistema
	 * @param dataInicial
	 * @param dataFinal
	 * @param idEmpresa
	 * @return
	 */
	public List<RelatorioProcedimento> relatorioProcedimentoMaisAcessadoAnalistaEspecificoData(Calendar dataInicial, Calendar dataFinal, Long idAnalista){
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where dataEmissao >= :dataInicial and dataEmissao <= :dataFinal and analista_id = :idAnalista and QtdProcedimentosAcessadosAnalista is not null", RelatorioProcedimento.class);
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("idAnalista", idAnalista);
		return query.getResultList();
	}
	
	/**
	 * tr�s todos os relatorio de procedimentos mais acessados por analista de uma empresa especifica 
	 * @param dataInicial
	 * @param dataFinal
	 * @param idAnalista
	 * @param idEmpresa
	 * @return
	 */
	public List<RelatorioProcedimento> relatorioProcedimentoMaisAcessadoAnalistaEspecificoPorEmpresaData(Calendar dataInicial, Calendar dataFinal, Long idAnalista, Long idEmpresa){
		TypedQuery<RelatorioProcedimento> query = manager.createQuery("from RelatorioProcedimento where dataEmissao >= :dataInicial and dataEmissao <= :dataFinal and analista_id = :idAnalista and empresa_id = :idEmpresa and QtdProcedimentosAcessadosAnalista is not null", RelatorioProcedimento.class);
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("idAnalista", idAnalista);
		query.setParameter("idEmpresa", idEmpresa);
		
		return query.getResultList();
	}

	
}

package baseDeConhecimento.dao;

import java.util.List;

import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Usuario;

public interface DaoCategoria extends DAO<Categoria> {

	public List<Categoria> buscarCategoriaPorEmpresa(Long id);
	
	public List<Categoria> buscarCategoriasPai();

	public List<Categoria> buscarNiveis(Long id);

	public Categoria buscarPorNome(String nome);

	public Categoria buscarPorNomeEEmpresa(String nome, Long id);
	
}

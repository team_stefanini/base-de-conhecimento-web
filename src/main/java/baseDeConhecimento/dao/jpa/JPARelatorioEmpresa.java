package baseDeConhecimento.dao.jpa;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import baseDeConhecimento.model.RelatorioEmpresa;
import baseDeConhecimento.model.RelatorioProcedimento;
import baseDeConhecimento.model.Usuario;

@Repository
public class JPARelatorioEmpresa {

	@PersistenceContext
	private EntityManager manager;

	public RelatorioEmpresa buscar(Long id) {

		return manager.find(RelatorioEmpresa.class, id);
	}

	public List<RelatorioEmpresa> buscarTodos() {
		TypedQuery<RelatorioEmpresa> query = manager.createQuery("from RelatorioEmpresa where status = true",
				RelatorioEmpresa.class);

		return query.getResultList();
	}

	public List<RelatorioEmpresa> buscarTodosInativos() {
		TypedQuery<RelatorioEmpresa> query = manager.createQuery("from RelatorioEmpresa where status = false",
				RelatorioEmpresa.class);

		return query.getResultList();
	}

	@Transactional
	public void inserir(RelatorioEmpresa obj) {
		manager.persist(obj);

	}

	@Transactional
	public void alterar(RelatorioEmpresa obj) {
		manager.merge(obj);

	}

	@Transactional
	public void deletar(RelatorioEmpresa obj) {
		manager.remove(buscar(obj.getId()));

	}

	@Transactional
	public void desativar(RelatorioEmpresa obj) {
		// TODO Auto-generated method stub

	}

	public List<RelatorioEmpresa> buscarRelatoriosGeraraisPorData(Date dataBuscada) {
		try {
			System.out.println("ENTREI NO TRY DO JPA >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where dataEmissao = :data and qntEmpresas is not null",
					RelatorioEmpresa.class);
			System.out.println("ACABEI DE FAZER A QUERY E A DATA � >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + dataBuscada);
			query.setParameter("data", dataBuscada);
			System.out.println("PASSEI DO SET PARAMETER E VOU IR PARA O RETURN");
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public RelatorioEmpresa buscarReltorioGeralEmpresa(Calendar cal, Usuario usuarioLogado) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and empresa is null",
					RelatorioEmpresa.class);
			query.setParameter("mes", cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * metodo para comparar se o relat�rio ja existe no banco 
	 * @param idUsuario
	 * @param idEmpresa
	 * @param mes
	 * @return
	 */
	public RelatorioEmpresa buscarRelatorioDoMes(Long idUsuario, Long idEmpresa, Calendar mes) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and empresa.id = :idEmpresa and usuarioGerouRelatorio.id = :idUsuario",
					RelatorioEmpresa.class);

			query.setParameter("mes", mes.get(Calendar.MONTH));
			query.setParameter("idEmpresa", idEmpresa);
			query.setParameter("idUsuario", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public RelatorioEmpresa buscarReltorioCategoriasPai(Calendar cal, Usuario usuarioLogado) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and qntCategoriasPai is not null",
					RelatorioEmpresa.class);
			query.setParameter("mes", cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public RelatorioEmpresa buscarReltorioGeralUsuario(Calendar cal, Usuario usuarioLogado) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and qntUsuarios is not null",
					RelatorioEmpresa.class);
			query.setParameter("mes", cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public RelatorioEmpresa buscarReltorioUsuariosNaEmpresa(Calendar cal, Usuario usuarioLogado, Long idEmpresa) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and empresa.id = :idEmpresa and qntUsuariosNaEmpresa is not null",
					RelatorioEmpresa.class);
			query.setParameter("mes", cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			query.setParameter("idEmpresa", idEmpresa);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public RelatorioEmpresa buscarReltorioProcedimentosNaEmpresa(Calendar cal, Usuario usuarioLogado, Long idEmpresa) {
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery(
					"from RelatorioEmpresa where month(dataEmissao) = :mes and usuarioGerouRelatorio.id = :idUsuario and empresa.id = :idEmpresa and qntProcedimentos is not null",
					RelatorioEmpresa.class);
			query.setParameter("mes", cal.get(Calendar.MONTH));
			query.setParameter("idUsuario", usuarioLogado.getId());
			query.setParameter("idEmpresa", idEmpresa);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Retorna um relatorio de uma empresa por data
	 * @param data
	 * @param idEmpresa
	 * @return RelatorioEmpresa
	 */
	public RelatorioEmpresa buscarRelatorioPorData(Calendar data, Long idEmpresa, Long idUsuario){
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery("from RelatorioEmpresa where empresa.id = :idEmpresa and month(dataEmissao) = :data and usuarioGerouRelatorio.id = :idUsuario", RelatorioEmpresa.class);
			query.setParameter("idEmpresa", idEmpresa);		
			query.setParameter("data", data.get(Calendar.MONTH));
			System.out.println("data >>>>>>>>>>>>>>>>>>>>>>>>>> " + data.get(Calendar.MONTH));
			query.setParameter("idUsuario", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * retorna relatorio geral
	 * @param data
	 * @param idUsuario
	 * @return
	 */
	public RelatorioEmpresa buscarRelatorioGeralPorData(Calendar data, Long idUsuario){
		try {
			TypedQuery<RelatorioEmpresa> query = manager.createQuery("from RelatorioEmpresa where empresa.id is null and month(dataEmissao) = :data and usuarioGerouRelatorio.id = :idUsuario", RelatorioEmpresa.class);
			query.setParameter("data", data.get(Calendar.MONTH));
			query.setParameter("idUsuario", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
	

}

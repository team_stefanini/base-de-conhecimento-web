package baseDeConhecimento.controller;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import baseDeConhecimento.dao.DaoCategoria;
import baseDeConhecimento.dao.DaoEmpresa;
import baseDeConhecimento.dao.DaoGrupoSolucao;
import baseDeConhecimento.dao.DaoProcedimento;
import baseDeConhecimento.dao.DaoUsuario;
import baseDeConhecimento.model.Categoria;
import baseDeConhecimento.model.Empresa;
import baseDeConhecimento.model.Imagem;
import baseDeConhecimento.model.Procedimento;
import baseDeConhecimento.model.TipoUsuario;
import baseDeConhecimento.model.Usuario;
import baseDeConhecimento.util.ArvoreCategoria;
import baseDeConhecimento.util.Util;
import baseDeConhecimento.view.InfoProcedimentos;

@CrossOrigin
@RestController
public class ProcedimentoController {
	@Autowired
	@Qualifier("JPAProcedimento")
	private DaoProcedimento daoProcedimento;

	@Autowired
	@Qualifier("JPAGrupoSolucao")
	private DaoGrupoSolucao daoGrupoSolucao;

	@Autowired
	@Qualifier("JPACategoria")
	private DaoCategoria daoCategoria;

	@Autowired
	@Qualifier("JPAEmpresa")
	private DaoEmpresa daoEmpresa;

	@Autowired
	@Qualifier("JPAUsuario")
	private DaoUsuario daoUsuario;

	@Autowired
	private ServletContext servletContext;

	// private boolean verificaCategoriaId = true;

	/*
	 * INSERINDO A IMAGEM
	 */

	@RequestMapping(value = "/procedimento/{id}/imagem", headers = ("content-type=multipart/*"), method = RequestMethod.POST)
	public ResponseEntity<FileInfo> uploadFoto(@RequestParam("file") MultipartFile[] inputFileImg,
			@PathVariable Long id) {
		FileInfo fileInfo = new FileInfo();
		HttpHeaders headers = new HttpHeaders();

		if (inputFileImg.length != 0 && id != null) {
			try {

				// Cria a pasta da empresa no servidor caso ela n�o exista.
				String caminhoDiretorio = servletContext.getRealPath("/");

				// busca o procedimento
				Procedimento procedimento = daoProcedimento.buscar(id);
				List<Imagem> imagens = new ArrayList<>();
				for (int i = 0; i < inputFileImg.length; i++) {

					String caminhoimagem = "/imgProcedimento/procedimento" + id + "." + i + ".jpg";

					File destinationFile = new File(caminhoDiretorio + caminhoimagem);

					if (!(inputFileImg[i].getContentType().contains("jpg")
							|| inputFileImg[i].getContentType().contains("jpeg"))) {
						return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
					}

					inputFileImg[i].transferTo(destinationFile);

					Imagem imagem = new Imagem();
					imagem.setCaminho(caminhoimagem);

					imagens.add(imagem);
				}

				procedimento.setImagem(imagens);
				daoProcedimento.alterar(procedimento);
				ArvoreCategoria.embaralhar();

				return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
		}
	}

	/*
	 * INSERINDO PROCEDIMENTO
	 */

	@RequestMapping(value = "/procedimento", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Procedimento> inserirProcedimento(@RequestBody Procedimento procedimento,
			HttpServletRequest request) {

		try {
			Usuario usuario = Util.getUsuarioLogado(request);

			// pode dar null pointer caso os atributos passados n�o exista
			// (categoria, grupo de solu��o) passada n�o exista

			if (!validarPermissoes(request, usuario, procedimento)) {
				return new ResponseEntity<Procedimento>(HttpStatus.UNAUTHORIZED);
			}

			// compara o titulo do procedimento
			Categoria categoriaProcedimento = daoCategoria.buscar(procedimento.getCategoria().getId());
			Procedimento procedimentoBusacado = daoProcedimento.buscarProcedimentoPorTitulo(
					categoriaProcedimento.getEmpresa().getId(), procedimento.getTitulo().trim());

			if (procedimentoBusacado != null) {
				return ResponseEntity.status(600).body(null);
			}

			// insere o usuario que cadastrou procedimento no procedimento
			procedimento.setUsuario(Util.getUsuarioLogado(request));
			// seta a vers�o inicial de todos os procedimentos
			procedimento.setVersao(new Long(1));

			daoProcedimento.inserir(procedimento);
			ArvoreCategoria.embaralhar();
			return ResponseEntity.created(URI.create("/procedimento/" + procedimento.getId())).body(procedimento);
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
			return new ResponseEntity<Procedimento>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Procedimento>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * ALTERAR PROCEDIMENTO
	 */

	@RequestMapping(value = "/procedimento", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<Procedimento> alterarProcedimento(@RequestBody Procedimento procedimento,
			HttpServletRequest request) {

		Procedimento procedimentoBuscado = daoProcedimento.buscar(procedimento.getId());

		Usuario usuarioLogado = Util.getUsuarioLogado(request);

		if (validarPermissoes(request, usuarioLogado, procedimento)) {

			try {
				procedimento.getGrupoSolucao().getId();
			} catch (NullPointerException e) {
				procedimento.setGrupoSolucao(null);
			}

			Categoria categoria = daoCategoria.buscar(procedimento.getCategoria().getId());
			if (procedimentoBuscado.getCategoria().getEmpresa().getId() != categoria.getEmpresa().getId()) {
				return ResponseEntity.status(603).body(null);
			}
			procedimento.setVersao(procedimentoBuscado.getVersao() + 1);
			procedimento.setUsuario(usuarioLogado);
			daoProcedimento.alterar(procedimento);
			ArvoreCategoria.embaralhar();
			return ResponseEntity.ok().body(procedimento);
		} else {
			return new ResponseEntity<Procedimento>(HttpStatus.UNAUTHORIZED);
		}
	}

	/*
	 * BUSCAR PROCEDIMENTO POR ID
	 */

	@RequestMapping(value = "/procedimento/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Procedimento> buscarProcedimento(@PathVariable Long id, HttpServletRequest request) {

		Usuario usuariologado = Util.getUsuarioLogado(request);
		Procedimento procedimento = daoProcedimento.buscar(id);
		if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return ResponseEntity.ok().body(daoProcedimento.buscar(id));
		}

		if (procedimento.isStatus() && usuariologado.getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
			// adiciona o usuario na lista de acesso somente quando for analista

			procedimento.getListaUsuario().add(usuariologado);
			daoProcedimento.alterar(procedimento);
			return ResponseEntity.ok().body(procedimento);
		} else {
			if (validarPermissoes(request, usuariologado, procedimento)) {
				return ResponseEntity.status(HttpStatus.OK).body(procedimento);
			}
			return null;
		}
	}

	/*
	 * BUSCAR TODOS OS PROCEDIMENTOS
	 */
	@RequestMapping(value = "/procedimento", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Procedimento> listarProcedimento(HttpServletRequest request) {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);

		if (!usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {
			return daoProcedimento
					.buscarProcedimentosEmpresaUsuarioLogado(Util.getEmpresasUsuarioLogado(usuarioLogado));
		}

		return daoProcedimento.buscarTodos();
	}

	/*
	 * BUSCAR TODOS OS PROCEDIMENTOS INATIVOS NO SISTEMA
	 */

	@RequestMapping(value = "/procedimento/inativos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Procedimento> listarProcedimentoInativos(HttpServletRequest request) {

		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		if (!(usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR))) {

			List<Categoria> categorias = new ArrayList<Categoria>();
			for (Empresa empresa : usuarioLogado.getEmpresa()) {
				categorias = daoCategoria.buscarCategoriaPorEmpresa(empresa.getId());
			}
			List<Long> categoriasId = new ArrayList<Long>();

			for (Categoria categoria : categorias) {
				String[] catFilhos = ArvoreCategoria.getFilhos(categoria.getId());
				populaLista(catFilhos, categoriasId);
			}

			List<Procedimento> procedimentos = daoProcedimento
					.buscarProcedimentosInativosRelacionadosPorEmpresa(categoriasId);

			if (procedimentos == null) {
				return null;
			}
		}
		return daoProcedimento.buscarTodosInativos();
	}

	/*
	 * METODO QUE DEVOLVE AS CATEGORIAS REQUISITADAS COM SEUS RESPECTIVOS
	 * PROCEDIMENTOS
	 */

	public static void populaLista(String vetor[], List<Long> lista) {
		for (String item : vetor) {
			lista.add(Long.parseLong(item));
			populaLista(ArvoreCategoria.getFilhos(Long.parseLong(item)), lista);
		}
	}

	@RequestMapping(value = "/procedimentoPorCategoria/{idCategoria}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<Procedimento> listarProcedimentoPorCategoria(@PathVariable Long idCategoria,
			HttpServletRequest request) {
		String[] catFilhos = ArvoreCategoria.getFilhos(idCategoria);
		List<Long> categorias = new ArrayList<>();
		populaLista(catFilhos, categorias);
		// if
		// (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ANALISTA))
		// {
		// return
		// daoProcedimento.buscarProcedimentosRelacionadosParaAnalista(idCategoria,
		// categorias);
		// } else {
		return daoProcedimento.buscarProcedimentosRelacionados(idCategoria, categorias);
		// }
	}

	@RequestMapping(value = "/procedimentoPorEmpresa/{idEmpresa}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody List<Procedimento> listarProcedimentoPorEmpresa(@PathVariable Long idEmpresa,
			HttpServletRequest request) {

		List<Categoria> categorias = daoCategoria.buscarCategoriaPorEmpresa(idEmpresa);
		List<Long> categoriasId = new ArrayList<Long>();

		if (Util.getUsuarioLogado(request).getEmpresa().contains(daoEmpresa.buscar(idEmpresa))
				|| Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR)) {

			for (Categoria categoria : categorias) {
				String[] catFilhos = ArvoreCategoria.getFilhos(categoria.getId());
				populaLista(catFilhos, categoriasId);
			}

			if (Util.getUsuarioLogado(request).getTipoUsuario().equals(TipoUsuario.ANALISTA)) {
				List<Procedimento> procedimentos = daoProcedimento
						.buscarProcedimentosRelacionadosPorEmpresaParaAnalista(categoriasId);

				if (procedimentos == null) {
					return null;
				}

				return procedimentos;
			} else {
				List<Procedimento> procedimentos = daoProcedimento
						.buscarProcedimentosRelacionadosPorEmpresa(categoriasId);

				if (procedimentos == null) {
					return null;
				}
				return procedimentos;
			}
		} else {
			return null;
		}
	}

	/*
	 * DELETAR PROCEDIMENTO
	 */

	@RequestMapping(value = "/procedimento/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> excluirProcedimento(@PathVariable Long id, HttpServletRequest request) {
		Procedimento procedimentoBuscado = daoProcedimento.buscar(id);
		Usuario usuarioLogado = Util.getUsuarioLogado(request);
		if (!(usuarioLogado.getTipoUsuario().equals(TipoUsuario.ADMINISTRADOR))) {
			if (usuarioLogado.getEmpresa().contains(procedimentoBuscado.getCategoria().getEmpresa())) {
				daoProcedimento.deletar(procedimentoBuscado);
				ArvoreCategoria.embaralhar();
				return ResponseEntity.noContent().build();
			} else {
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
			}
		}
		daoProcedimento.deletar(daoProcedimento.buscar(id));
		ArvoreCategoria.embaralhar();
		return ResponseEntity.noContent().build();
	}

	/*
	 * DESATIVAR PROCEDIMENTO
	 */

	@RequestMapping(value = "/procedimento/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> desativarProcedimento(@PathVariable Long id) {
		daoProcedimento.desativar(daoProcedimento.buscar(id));
		ArvoreCategoria.embaralhar();
		return ResponseEntity.noContent().build();
	}

	/*
	 * METODO QUE BUSCA TODAS OS PROCEDIMENTOS ELEGIVEIS
	 */

	@RequestMapping(value = "/procedimento/elegivel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Procedimento> listarProcedimentosElegiveis() {
		return daoProcedimento.buscarLegiveis();
	}

	/*
	 * METODO QUE BUSCA TODAS OS PROCEDIMENTOS ILEGIVEIS
	 */

	@RequestMapping(value = "/procedimento/ilegivel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<Procedimento> listarProcedimentosIlegiveis() {
		return daoProcedimento.buscarIlegiveis();
	}

	/**
	 * Aprova o procedimento (seta a propriedade status para true )
	 * 
	 * @param idProcedimento
	 * @return
	 */
	@RequestMapping(value = "/procedimento/aprovacao", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Void> aprovarProcedimento(@RequestBody InfoProcedimentos infoProcedimentos) {
		try {
			Procedimento procedimentoBuscado = daoProcedimento.buscarInativos(infoProcedimentos.getId());
			procedimentoBuscado.setStatus(true);
			daoProcedimento.alterar(procedimentoBuscado);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/*
	 * METODO QUE VERIFICA QUAIS PROCEDIMENTOS N�O EST�O VALIDOS
	 */

	// @Scheduled = fixedRate fala de quanto tempo o m�todo deve ser executado
	// em millisegundos
	@Scheduled(fixedRate = 86400000)
	public void verificarProcedimentos() {
		List<Procedimento> procedimentos = daoProcedimento.buscarTodos();
		Date dataAtual = new Date();
		if (procedimentos.size() > 0) {
			for (Procedimento procedimento : procedimentos) {
				if (dataAtual.getTime() > procedimento.getVencimento().getTime()) {
					daoProcedimento.deletar(procedimento);
				}
			}
		}

	}

	// Verifica se o coodenador esta atualizando os procedimento pertinentes as
	// empresas que ele coordena :)
	private boolean validarPermissoes(HttpServletRequest request, Usuario usuario, Procedimento procedimento) {
		if (usuario.getTipoUsuario().equals(TipoUsuario.COORDENADOR)) {

			Categoria categoriaProcedimento = daoCategoria.buscar(procedimento.getCategoria().getId());
			Empresa empresaProcedimento = daoEmpresa.buscar(categoriaProcedimento.getEmpresa().getId());

			if (usuario.getEmpresa().contains(empresaProcedimento)) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
}
